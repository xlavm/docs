# Estabilidad (Optimize Routing)

Para darle estabilidad al servicio, se usa el `Enable Application Optimized Routing`, esto lo que evita es que envie una peticion por una WAN y reciba la respuesta por otra WAN diferente.

Para darle estabilidad "priorizando WAN", se activa el `Enable Bandwidth Based Balance Routing on port(s)` especificando los puestos, esto significa que si tengo las siguientes WAN:

- WAN1: 20MB
- WAN2: 10MB

el trafico lo va a priorizar en la WAN1 que es la que tiene mayor ancho de banda

![stability](img/stability.png)
