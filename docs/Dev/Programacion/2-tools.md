# Herramientas

## IDE

Es la sigla de Entorno de Desarrollo Integrado. Nos sirve a los desarrolladores para tener un editor de codigo, herramientas de automatizacion de compilacion y un depurador de codigo.

Podemos tener muchos y algunos se especializan en algunas tecnologias, frameworks o lenguajes.

**Ejemplos**:

| IDE                       | Tecnologia         |
| ------------------------- | ------------------ |
| `Visual Studio Code`      | multi-tech         |
| `Microsoft Visual Studio` | .NET               |
| `Netbeans`                | Java, PHP          |
| `Eclipse`                 | Java               |
| `IntelliJ`                | Java               |
| `PyCharm`                 | Python             |
| `GNU Octave`              | MatLab             |
| `Spring Tools`            | Spring Framework   |
| `MySQL`                   | MySQL Workbench    |
| `SQL Server`              | Management Studio  |

**Nota:** Estos son los que he usado con las tecnologias descritas, sin embargo prefiero un IDE multi-tech como *Visual Studio Code*.

## Dibujo Grafico

Este tipo de herramienta nos ayuda a crear diagramas para las diferentes soluciones de software tales como:

- Diagramas de Flujo
- UML
- Entidad Relacion
- Diagramas de Arquitecturas
- Diagramas de Infraestructura
- Diagramas BPM
- Entre otros...

**Ejemplos**:

| Herramienta       | Diagrama           |
| ----------------- | ------------------ |
| `Draw.io`         | multi-diagrama     |
| `Microsoft Visio` | multi-diagrama     |
| `Star UML`        | Diagramas UML      |
| `Bizagi`          | Diagramas BPM      |

**Nota:** Estas son las que he usado con los diagramas descritos, sin embargo prefiero una herramienta multi-daigrama como *Draw.io*; a comparacion de Microsoft Visio, Draw.io es mas agil y versatil.
