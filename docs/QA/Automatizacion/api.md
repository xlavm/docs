# Api

Tecnologias:

- Axios
- Cypress

**Documentos de apoyo**

- Axios: <https://axios-http.com/es/docs/intro>

- Cypress:<https://docs.cypress.io/guides/overview/why-cypress>

## Frameworks Base

<https://gitlab.com/lavm/qaa-frameworks/-/tree/main/frameworks-bases/api?ref_type=heads>
