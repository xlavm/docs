# Windows

Vamos a darle estabilidad al PC y priorizaremos la GPU para que sea mas eficiente en tareas que requieran de alto rendimiento.

Mejorar el rendimiento e Input Lag:

## Administrador de dispositivos

Vamos a  `Dispositivos del sistema` y deshabilitamos el `Temporizador de eventos de alta precision`

![temporizador-eventos](img/temporizador-eventos.png)

## Editor de registros

Vamos a la ruta: `Equipo\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Multimedia\SystemProfile`

Cambiamos la capacidad de respuesta del sistema

![er1](img/er1.png)

Se despliega la carpeta `Tasks` luego vamos a `Games` y configuramos:

![er2](img/er2.png)

Tomado de:

<https://www.youtube.com/watch?v=LClABi8nUgM&list=WL&ab_channel=RedSettings>

## Opciones de Energia

Abrimos el CMD como administrador, ejecutamos el siguiente comando para habilitar el Plan de energía Máximo Rendimiento (solo PC de sobremesa de gama alta) y en opciones de energia seleccionamos el plan de Maximo Rendimiento

```BASH
powercfg -duplicatescheme e9a42b02-d5df-448d-aa00-03f14749eb61
```

![opciones-energia](img/opciones-energia.png)
