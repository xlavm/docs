    public class Principal{
        public static void main(String[] args) {
            Animal miAnimal = new Perro(); //aquí le decimos que toma forma de un perro
            miAnimal.dice();
        }
    }

    public class Animal{
        public void dice(){
            System.out.println("que ruido puedo hacer?");
        }
    }

    public class Perro extends Animal{
        @Override
        public void dice(){
            System.out.println("Guau!!");
        }
    }

    public class Gato extends Animal{
        @Override
        public void dice(){
            System.out.println("miau!!");
        }
    }
    //output: Guau!!