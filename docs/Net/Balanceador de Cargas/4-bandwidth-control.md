# Control de Ancho de Banda

Identifico los dispositivos de menos prioridad para agruparlos y controlarles el ancho de banda

![devices](img/devices.png)

Con la MAC Address consulto sus IP en el balanceador de carga y le doy en guardar para reservarles IP Staticas

![search-devices](img/search-devices.png)

Reservo las IP para los dispositivos guardados desde el 100 hasta el 106 para posteriormente crear un rango de direcciones IP

![ip-reservadas](img/ip-reservadas.png)

Creo el rango de direcciones IP 100 - 106 y lo llamo: `RangoDispositivosSecundarios`

![rango-disp-sec](img/rango-disp-sec.png)

Creo el grupo con el rango anterior y le llamo `GrupoDispositivosSecundarios`

![grupo-disp-sec](img/grupo-disp-sec.png)

Ahora asigno el ancho de banda maximo para el grupo creado (500Kbps)

![asig-ancho-banda](img/asig-ancho-banda.png)

Activo el control de ancho de banda

![act-control-ancho-banda](img/act-control-ancho-banda.png)
