# Variables de Entorno

Instala la el paquete o dependencia `dotenv` para react

```BASH
npm install react-native-dotenv --save
npm i babel-install --save
npm i @babel/preset-react --save
```

Configuras el paquete, creas el archivo `.babelrc` y agregas la configuracion

```JSON
{
    "presets": ["@babel/preset-react"],
    "plugins": [
      ["module:react-native-dotenv", {
        "envName": "APP_ENV",
        "moduleName": "@env",
        "path": ".env"
      }]
    ]
}
```

Creas el archivo `.env` en la raiz del proyecto y dentro ubicas las variables

```BASH
API_PATH=https://luisvanegas.com
```

Usas la variable en tu proyecto

```JS
import {API_PATH} from "@env"
const API_PATH_HERE = API_PATH
```
