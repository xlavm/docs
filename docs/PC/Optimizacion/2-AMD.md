# AMD

Se usuara el software `AMD Adrenalin Edition` para hacer las configuraciones

## Sistema

![amd-sistema](img/amd-sistema.png)

## Graficos

- Superresolucion Radeon (RSR): Despues de activar, se debe ir a los juegos y bajar la resolucion por ejemplo a 720p para que RSR lo renderize a 1080p y asi podre jugar 1080p con mas FPS
- Radeon Enhanced Sync: Minimiza la ruptura visual y la demora pero no limita los FPS y funciona con FreeSync (Si tienes monitor compatible con FreeSync, activalo)

![graficos1](img/graficos1.png)

### Opciones Avanzadas

![graficos-avanzados](img/graficos-avanzados.png)

## Pantalla

- AMD Freesync: La frecuencia de actualizacion del monitor se sincronizara con los FPS de la GPU eliminando `Input Lag`, `Interrupciones e Inestabilidad de Frames` Si tu monitor es compatible, activalo
- Mejora de Color de la Pantalla: esto para ver mejor los enemigos que se encuentran muy camuflados

![pantalla](img/pantalla.png)

## Rendimiento

Vamos a estabilizar la GPU para un mayor rendimiento

Cuando hay juegos que demandan de mucha CPU, la GPU se pone perezosa y deja que la CPU haga el trabajo, lo cual genera bajones de FPS e input lag. entonces la frecuencia minima se sube al valor de frecuencia maxima restado 300 o 800. en mi caso le resto 300 y da: 2,269

El limite de energia se lleva al maximo para que las frecuencias sean mas estables.

Se activa el AMD SmartAccess Memory, esto se permite cuando se ha activado previamente en la BIOS.

![rendimiento](img/rendimiento.png)

Tomado de:

<https://www.youtube.com/watch?v=LClABi8nUgM&list=WL&ab_channel=RedSettings>
