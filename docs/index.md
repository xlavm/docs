# Manifiesto

Esta documentacion ha sido desarrollada con la ayuda de MKDOCS <https://www.mkdocs.org/> y MKDOCS-MATERIAL <https://squidfunk.github.io/mkdocs-material/reference/>

Por **Luis Vanegas - Senior QA Automation Engineer 🦾 | DevOps 🚀 | Senior Developer 👨🏻‍💻**.
