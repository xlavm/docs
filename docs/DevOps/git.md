# Git

## Sistema de Control de Versiones (SCV)

Son herramientas de software que ayudan a los equipos de software a gestionar los cambios en el código fuente a lo largo del tiempo.

## Definicion de Git

Es el sistema de control de versiones moderno más utilizado del mundo. Git es un proyecto de código abierto maduro y con un mantenimiento activo que desarrolló originalmente Linus Torvalds, el famoso creador del kernel del sistema operativo Linux, en 2005.

## Estados de Git

- **Working directory:** editamos y trabajamos en el proyecto sin usar git
- **Staging área:** elegimos qué archivos están 100% listos para seguir al siguiente estado
- **Repositorio:** usamos el control de versiones de git, donde nuestro proceso estará guardado a través de commits.

## Configuración Inicial

Si trabajamos desde **Windows**: instalamos git y desde git bash o cmd verificamos la versión con el comando `git --version`.

Si trabajamos desde el **WSL** de Windows no es necesario instalar git

Si trabajamos desde **Linux** o **Mac**, tal vez no es necesario instalar git

Se crea la configuracion inicial desde el Bash o CMD:

```BASH
git config --global user.name 'Luis Vanegas'
git config --global user.email 'angelvamart@hotmail.com'
```

- si vamos a trabajar con un repo de un SCV:

    Clonamos el repo en el directorio donde deseamos tenerlo:

    ```BASH
    git clone https://urldelclone.git
    ```

- Si vamos a trabajar localmente:
    Creamos una carpeta donde queremos guardar el repo; dentro de la carpeta y desde el bash digitamos el comando `git init`.

## Comandos

### Comunes

| Comando                                | Descripcion                                        |
| -------------------------------------- | -------------------------------------------------- |
| `git status`                           | ver estado del repositorio                         |
| `git add .`                            | agregar todos los cambios en memoria               |
| `git commit -m 'este es mi commit'`    | crear commit para los cambios guardados en memoria |
| `git push origin <rama>`               | subir cambios a repositorio remoto en la rama      |
| `git pull origin <rama>`               | bajar cambios de repositorio remoto en la rama     |

### Ramas

| Comando                                | Descripcion                                        |
| -------------------------------------- | -------------------------------------------------- |
| `git branch`                           | ver ramas                                          |
| `git branch -r`                        | ver ramas remotas                                  |
| `git branch <rama>`                    | crear rama                                         |
| `git checkout <rama>`                  | cambiar de rama                                    |
| `git checkout -b <rama>`               | crear rama y cambiarse a ella                      |
| `git branch -D <rama>`                 | eliminar rama local                                |
| `git push origin :<rama>`              | eliminar rama remota                               |
| `git log --oneline --decorate`         | ver donde apunta cada rama                         |

### Eliminar Cambios

| Comando                                | Descripcion                                        |
| -------------------------------------- | -------------------------------------------------- |
| `git reset --hard HEAD~1`              | eliminar el ultimo commit hecho                    |
| `git checkout -- .`                    | eliminar ultimo cambio no guardado                 |
| `git checkout -- ruta/archivo`         | eliminar cambios especificos                       |

## .gitignore

| Instruccion                            | Descripcion                                        |
| -------------------------------------- | -------------------------------------------------- |
| `**`                                   | ignorar todos los archivos                         |
| `*/*`                                  | ignorar todos los archivos de cualquier carpeta    |
| `*.*`                                  | ignorar todos los archivos de cualquier formato    |
| `*.txt`                                | ignorar solo archivos con extensión txt            |
| `**/.terraform`                        | ignorar .terraform de cualquier directorio         |
| `terraform.*`                          | ignorar cualquier archivo llamado terraform        |
| `/**`                                  | ignorar todos los archivos                         |
| `!*.tfvars`                            | NO ignorar los archivos con extencion tfvars       |
