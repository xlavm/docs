# Virtualbox

## Descarga

<https://www.virtualbox.org/wiki/Downloads>

Se debe descargar e instalar el Virtualbox con el Extension pack

NOTA: El extensión pack y el host de virtualbox deben ser los actuales y de misma versión.

## Configurar Extension Pack

![extension-pack1](img/virtualbox/extension-pack1.png)

![extension-pack2](img/virtualbox/extension-pack2.png)

## Crear Maquina Virtual

![create-machine1](img/virtualbox/create-machine1.png)

![create-machine2](img/virtualbox/create-machine2.png)

![create-machine3](img/virtualbox/create-machine3.png)

![create-machine4](img/virtualbox/create-machine4.png)

## Extension Pack para Full Screen

![full-screen1](img/virtualbox/full-screen1.png)

![full-screen2](img/virtualbox/full-screen2.png)

![full-screen3](img/virtualbox/full-screen3.png)

![full-screen4](img/virtualbox/full-screen4.png)

NOTA: Reinicia la máquina.

## Exportar Maquina .ova

![export1](img/virtualbox/export1.png)

![export2](img/virtualbox/export2.png)

![export3](img/virtualbox/export3.png)

![export4](img/virtualbox/export4.png)

## Importar Maquina .ova

![import1](img/virtualbox/import1.png)

![import2](img/virtualbox/import2.png)

![import3](img/virtualbox/import3.png)
