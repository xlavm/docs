# Angular Universal

Es una tecnologia que nos permite ejecutar la aplicacion del lado del servidor `Server Side Rendering`

## Agregar Angular Universal

```BASH
ng add @nguniversal/express-engine
```

Para mas informacion: <https://angular.io/guide/universal>

## Ejecuta