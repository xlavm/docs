# Empezando

## Instalar

Ir al sitio <https://nodejs.org/en/> y descargar la version LTS

Verificas las instalaciones

```BASH
node -v
```

```BASH
npm -v
```

## Crear Proyecto

Creas la carpeta donde estara tu proyecto y dentro ejecutas:

```BASH
npm init
```

Configura el `package.json`

```JSON
{
  "description": " ",
  "author": "Luis Vanegas angelvamart@hotmail.com (xlavm)",
  "license": "ISC",
  "main": "server.js",
  "scripts": {
    "start": "node server.js"
  },
  "keywords": [
    "Node",
    "Express",
    "Mongoose"
  ],
  "repository": {
    "type": "git",
    "url": "luisvanegas.com"
  }
}
```

## Ejecutar Proyecto

Te situas dentro de la carpeta del proyecto y ejecuta

```BASH
npm i
```

```BASH
npm start
```

## Estructura del proyecto

```BASH
src/                # Carpeta con codigo fuente
    models/
    controllers/
    routes/
test/               # Carpeta con las pruebas 
package.json        # Configuraciones del Proyecto
```

## Archivos Principales

- package.json
- src/
