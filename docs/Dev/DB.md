# DB's

## Firestore

1. crear proyecto en `Firebase`
2. configurar el proyecto asignando ambiente y creando aplicacion

    ![config](img/config.png)

    ![config2](img/config2.png)

3. usa la siguiente informacion para conectarte programaticamente

    ![config3](img/config3.png)

4. crea una base de datos para empezar a trabajar

    ![createdb](img/createdb.png)

### Conexion por serviceAccountKey.json

1. ve a la siguiente opcion y genera la clave privada

    ![serviceaccount](img/serviceaccount.png)
