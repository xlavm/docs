# Conceptos

## Validación y Verificación

- **Validación:** confirmación mediante examen y evidencia de que se haya cumplido los requisitos para un proposito en especifico *“establece si se está construyendo el producto correcto”*
- **Verificación:** confirmación mediante examen y presentación de pruebas de que se han cumplido los requisitos especificos. *“establece si se está construyendo de forma correcta”*

## Version y Release

- **Versión:** modificaciones mayores con respecto a arquitectura
- **Release:** modificaciones pequeñas o corrección de defectos

## Calidad del Software (QA)

Es el grado de cumplimientos de las carecteristicas de un software

Puntos de vistas de la calidad del software:

- Usuario
- Fabricante
- Producto: funciones y caracteristicas
- Basado en el valor: lo que el cliente está dispuesto a pagar

## Pruebas de Software

Son aquellas que ayudan a detectar errores en las fases previas del proyecto y tiene como objetivos:

- *detectar el mayor numero de errores.*
- *reducir el numero de casos de pruebas.*

## Principios de Pruebas de Software

- Las pruebas muestran la prensencia de defectos, “si no se encuentran defectos, no es una prueba de que no existan defectos”.
- Las pruebas exhaustivas son imposibles: probar todo no es factible. En vez de las pruebas exhaustivas, usamos riesgos y prioridades para enfocar los esfuerzos de una prueba.
- Pruebas tempranas, las pruebas deben empezar lo más temprano en el ciclo de vida del desarrollo y deben enfocarse en objetivos definidos.
- Agrupación de defectos.
- Paradoja del pesticida: si las pruebas se repiten una y otra vez, probablemente el mismo conjunto de casos de pruebas ya no encuentren ningun nuevo error.
- La prueba depende del contexto, las vulnerabilidades de un contexto no pueden ser las mismas de otro contexto diferente
- Falacia de ausencia de errores. Siempre hay uno.

## Enfoques de Pruebas

- **QA Constructivo:** enfoque en la calidad de los procesos de desarrollo de software (incluyendo el testing), entre ellos los métodos, herramientas, guias, estándares, requisitos legales, lenguaje de programación.
- **QA Analitico:** enfoque en la calidad de los productos del desarrollo de software (es lo que conocemos como pruebas)
    1. Pruebas Dinámicas: se realiza al código en ejecución, como son pruebas de cajas negras y blancas y pruebas basadas en la experiencia.
    2. Pruebas Estáticas: se realiza a los entregables del CVDSA que no requiere ejecución de código. Como son documentos, diagramas, mockups.
    3. Pruebas Convencional: se empieza probando el modulo (componente) y después la integración de modulos
    4. Pruebas Orientadas a Objetos (POO): Se prueba primero sus clases, atributos y metodos.

## Procesos de Pruebas

- **Planificación y control:** establece objetivos y metas. Control: compara el progreso con la planificación.
- **Análisis y diseño:** transforma los objetivos en tareas tales como revisar la base de pruebas, requisitos y análisis de riesgo dando prioridades y por ultimo el diseño de los casos de pruebas.
- **Ejecución:** se especifica los procedimientos. Verificar que el entorno de prueba haya sido debidamente configurado, implementar los casos de pruebas y registrar los resultados de la ejecución de casos de pruebas.
- **Evaluación:** evalua los criterios de salida y los informes y compara la ejecución contra los objetivos definidos. Evalua si se requiere más pruebas.
- **Actividad de cierre:** se recopila los datos de las pruebas terminadas y se realizan las siguientes labores:
    1. Comprobar los documentos entregados
    2. Cerrar los informes de incidencias
    3. Documentar cuantos usuarios aceptan el sistema
    4. Archivar los productos de soporte, entorno y la infraestructura para usuarios en distintas pruebas
    5. Utilizar la información recopilada para mejorar la madurez de las pruebas

## Tipos de Pruebas

- **Pruebas funcionales:** verifica que cada función del software opere conforme a las especificaciones
- **Pruebas no funcionales:** verifica que el software funciona bien, su fiabilidad, rendimiento, disponibilidad, etc.
- **Pruebas de caja blanca:** se basa en el funcionamiento de código, verifica fallas en la seguridad interna, la funcionalidad de los bucles condicionales, etc.
- **Pruebas de caja negra:** verifica la funcionalidad sin examinar el código, solo validan los datos de entrada y los datos de salida.
- **Pruebas de regresión:** se ejecutan para confirmar que los cambios hechos en el código no han afectado otras funciones
- **Pruebas de humo:** es un grupo de casos de pruebas que establece que el sistema es estable y que toda funcionalidad está presente y funciona en condiciones normales.
- **Pruebas unitarias:** son aquella que se le hacen a bloques de codigo, funciones, metodos, etc.
- **Pruebas de integracion:** son aquellas que se le hacen a un conjunto de funciones, metodos u bloques de codigos trabajando entre si.
- **Retest:** consiste en probar y si se encuentra un bug, dárselo al desarrollador para que lo corrija y cuando él lo regrese con la corrección, volverlo a probar.
- **E2E:** consiste en probar a nivel de usuario final la aplicacion de inicio a fin.

## ISO 25010

<https://iso25000.com/index.php/normas-iso-25000/iso-25010>

![iso](https://iso25000.com/images/figures/iso25010_c.png)

## TDD (Test Driven Development)

Es una práctica de programación que consiste en escribir primero las pruebas (generalmente unitarias), después escribir el código fuente que pase la prueba satisfactoriamente y, por último, refactorizar el código escrito.

## BDD (Behavior Driven Development)

Es una estrategia de desarrollo dirigido por comportamiento, que ha evolucionado desde TDD. A diferencia de TDD, BDD se define en un idioma común entre todos los stakeholders, lo que mejora la comunicación entre equipos tecnológicos y no técnicos. Tanto en TDD como en BDD, las pruebas se deben definir antes del desarrollo, aunque en BDD, las pruebas se centran en el usuario y el comportamiento del sistema, a diferencia del TDD que se centra en funcionalidades.

## Actividades QA en Scrum

### Proceso QA

- Realizar plan de pruebas
- Diseño de casos de prueba
- Ejecución de pruebas
- Automatización de pruebas
- Registro y seguimiento de incidencias
- Pruebas de regresión
- Toma de evidencias
- Realizar certificación (Carta formal)

### Ceremonias Scrum

- Inception
    1. Conocer el negocio y el cliente para el cual se desarrollará el proyecto
    2. Entender cuál es la necesidad y qué valor se desea generar al usuario final con la implementación del producto
    3. Conocer cuál es el perfil/tipo de usuario para el cual se desarrollará el producto.

- Socialización
    1. Entender las funcionalidades y los flujos que maneja el cliente con su usuario final
    2. Entender el valor que se desea generar con el desarrollo del proyecto, en la relación cliente-usuario

- Priorización
    1. Conocer el backlog y las funcionalidades que tendrá el producto (portal, app, etc) y estas deben estar priorizadas (MPV)
    2. Comenzar a validar el DoR (Definition of ready) de las Historias de usuario.

- Refinamiento
    1. Conocer los flujos de los procesos que se manejarán en el desarrollo del proyecto
    2. Validar que están listos todos los insumos necesarios para desarrollar cada Hu’s (DoR)
    3. Validar que el diseño de UX/UI cumpla con lo definido en el proyecto hasta el momento.

- Planning
    1. Participar en la definición detallada de los criterios de aceptación de cada HU’s
    2. Comentar las dudas que se tengan sobre las HU’s e identificar validaciones que no se hayan tenido en cuenta
    3. Validar que el diseño presentado por UX/UI finalmente si cumpla con lo definido en las HU’s
    4. Identificar los tipos de usuario y demás datos que necesitemos para ejecutar los diferentes escenarios de prueba
    5. Identificar si es necesario generar un documento de consideraciones generales (Diseño, validaciones de campos, caracteres permitidos,etc) para tener en cuenta durante todo el desarrollo
    6. Generar un caso de prueba por cada HU’s, comentarla como una tarea y socializarla con los desarrolladores
    7. Participar en la votación de puntos para cada HU’s (Estimación)

- Daily
    1. Comentar ¿Qué hice ayer? ¿Qué voy a hacer hoy? Y ¿Qué impedimentos tengo?
    2. Informar como va la ejecución del proceso de pruebas. (teniendo en cuenta versiones, ambientes, etc).
    3. Comentar nuevos casos o validaciones que hayamos identificado y que deban ser contempladas en el desarrollo del proyecto.

- Review
    1. Presentar la Review de al menos uno de los sprint del proyecto
    2. Tomar evidencias de lo desarrollado en el sprint
    3. Socializar los incidentes que se hayan presentado en el sprint (Indicadores)
    4. Socializar la automatización de pruebas que se haya realizado (Si aplica)
    5. Tomar nota de las observaciones o conclusiones que se presenten en la ceremonia para poder gestionarlas posteriormente.

- Retrospectiva
    1. Identificar posibilidades de mejora en cuanto a personas, relaciones, procesos y herramientas, para implementar en el próximo sprint.
    2. Buscar mejores formas de trabajar.
    3. Hablar sobre la información que salió en la Review si es necesario.
