# Empezando

## Instalar

**Prerrequisitos**

- Node JS
- NPM

Instala Angular

```BASH
npm install -g @angular/cli@latest
```

```BASH
ng version
```

## Crear Proyecto

=== "Crear Proyecto"

    ```BASH
    ng new <nombre_de_proyecto>
    ```

=== "Componentes"

    ```BASH
    ng g c components/<nombre_componente> 
    ```

=== "Servicios"

    ```BASH
    ng g s services/<nombre_servicio> 
    ```

=== "Modulos"

    ```BASH
    ng g m components/<nombre_componente> 
    ```

=== "Guards"

    ```BASH
    ng g guard guards/<nombre_guard> 
    ```

=== "Clases"

    ```BASH
    ng generate class models/<nombre_modelo>
    ```

Configura el `package.json`

```JSON
{
  "description": " ",
  "author": "Luis Vanegas angelvamart@hotmail.com (xlavm)",
  "license": "ISC",
  "scripts": {
    "ng": "ng",
    "start": "ng serve",
    "build": "ng build"
  },
  "keywords": [
    "Angular",
    "Bootstrap"
  ],
  "repository": {
    "type": "git",
    "url": "luisvanegas.com"
  }
}
```

## Ejecutar Proyecto

```BASH
npm i
```

```BASH
npm start
```

## Estructura del proyecto

```BASH
src/                # Carpeta con codigo fuente
    app/            # Carpeta de archivos de la app
        app-routing.module.ts
        app.component.ts
        app.module.ts
    assets/         # Recursos de la app
    environments/   # Carpeta con archivos de variables de entorno
    main.ts
    polyfills.ts
    styless.css     # Hoja de estilos de la app
angular.json        # Configuraciones de Angular
package.json        # Configuraciones del Proyecto
tsconfig.json       # Configuraciones de Typescript
tsconfig.app.json   # Configuraciones de Typescript para app
```

## Archivos Principales

- src/
- package.json
- angular.json
- tsconfig.json
- tsconfig.app.json
- polyfills.ts
- main.ts
- styless.css
- app-routing.module.ts
- app.component.ts
- app.module.ts
