# Sumar Cargas (Bonding)

Esto lo que hace es que si tienes 2 ISP con los siguientes servicios:

- ISP1: 20MB
- ISP2: 10MB

A la final los dispositivos que esten conectados a tu red por medio del Load Balancer van a tener 30MB.

## Configuracion

Entra a tu Load Balancer <http://192.168.2.1/webpages/login.html> y configura la siguiente ventana de tal forma que se vea asi:

![bonding](img/bonding.png)
