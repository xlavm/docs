# Carga Diferida

Retrasa la carga de un modulo hasta que realmente el usuario lo necesite, esto aumenta el performance de la aplicacion.

Este es un resultado de ejemplo:

Se pasaron de tener `2.683ms` a tener `205ms` en tiempos de carga

![result](img/result.png)

Para agregar lazy load a un componente, se ejecuta el siguiente comando, el cual cargara los modulos para el componente especifico, en este caso: `home`

```BASH
ng g m <ruta_componentes>/<nombre_carpeta_del_componente> --routing true

#ejemplo:
ng g m components/home --routing true
```

Modifica los siguientes scripts para el modulo `home`

=== "home.module.ts"

    ```TS
    import { NgModule } from '@angular/core';
    import { CommonModule } from '@angular/common';
    import { HomeRoutingModule } from './home-routing.module';
    import { HomeComponent } from './home.component';


    @NgModule({
      declarations: [HomeComponent], //agrego el HomeComponente 
      imports: [
        CommonModule,
        HomeRoutingModule
      ]
    })
    export class HomeModule { }
    ```

=== "home-routing.module.ts"

    ```TS
    import { NgModule } from '@angular/core';
    import { RouterModule, Routes } from '@angular/router';
    import { AuthGuard } from 'src/app/guards/auth.guard';
    import { HomeComponent } from './home.component';

    const routes: Routes = [//traigo esta ruta desde el app-routing.module.ts
      { path: '', component: HomeComponent, canActivate: [ AuthGuard ] },//guard
    ];

    @NgModule({
      imports: [RouterModule.forChild(routes)],
      exports: [RouterModule]
    })
    export class HomeRoutingModule { }
    ```

=== "app-routing.module.ts"

    Modifico la ruta, para que quede asi

    ```TS
      { path: 'home', loadChildren: () =>  import ('./components/home/home.module').then(m => m.HomeModule)},//guard
    ```

=== "app.module.ts"

    En este script, desde la seccion de `declarations: []` elimino el `HomeComponent`

---

## Aplicando en Formularios Reusables

Se van a manejar 3 componentes, el `Form` que sera el formulario a reusar y los componentes `SignIn` y `SignUp` que son los componentes que usan a `Form`.

Se crean los modulos para los 3 componentes

```BASH
ng g m components/form --routing true
ng g m components/sign-in --routing true
ng g m components/sign-up --routing true
```

En el componente `Form` hacemos los respectivos cambios en los modulos

=== "form.module.ts"

    - Importo los modulos que requiero en la seccion `imports:[]` para que el formulario funcione

    - Declaro el componente: `declarations: [FormComponent]`

    - Exporto el componente: `exports:[FormComponent]` para que lo use otros componentes

    ```TS
    import { NgModule } from '@angular/core';
    import { CommonModule } from '@angular/common';
    import { FormRoutingModule } from './form-routing.module';
    import { FormComponent } from './form.component';
    import { ReactiveFormsModule } from '@angular/forms';

    @NgModule({
      declarations: [FormComponent],
      exports:[FormComponent],
      imports: [
        CommonModule,
        FormRoutingModule,
        ReactiveFormsModule //para formularios reactivos
      ]
    })
    export class FormModule { }
    ```

=== "sign-in.module.ts"

    Aqui se importa el modulo del formulario que se esta reutilizando, en la seccion `imports:[]`

    ```TS
    import { NgModule } from '@angular/core';
    import { CommonModule } from '@angular/common';
    import { SignInRoutingModule } from './sign-in-routing.module';
    import { SignInComponent } from './sign-in.component';
    import { FormModule } from '../form/form.module';

    @NgModule({
      declarations: [SignInComponent],
      imports: [
        CommonModule,
        SignInRoutingModule,
        FormModule,
      ]
    })
    export class SignInModule { }
    ```

=== "sign-up.module.ts"

    Aqui se importa el modulo del formulario que se esta reutilizando, en la seccion `imports:[]`

    ```TS
    import { NgModule } from '@angular/core';
    import { CommonModule } from '@angular/common';
    import { SignUpRoutingModule } from './sign-up-routing.module';
    import { SignUpComponent } from './sign-up.component';
    import { FormModule } from '../form/form.module';

    @NgModule({
      declarations: [SignUpComponent],
      imports: [
        CommonModule,
        SignUpRoutingModule,
        FormModule
      ]
    })
    export class SignUpModule { }
    ```

=== "app.module.ts"

    En este script, desde la seccion de `declarations: []` elimino los componentes a los cuales les cree modulos independientes (Form, SignIn, SignUp)