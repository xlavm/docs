# Navegar entre pantallas

Instala las siguientes dependencias (Entorno Android con npm)

```BASH
npm install @react-navigation/native @react-navigation/native-stack --save
npm install react-native-screens react-native-safe-area-context --save
```

=== "App.js"

    ```JS
    import React from 'react';
    import SignIn from './components/sign-in'
    import Home from './components/home'

    import { NavigationContainer } from '@react-navigation/native';//para navigation
    import { createNativeStackNavigator } from '@react-navigation/native-stack';//para navigation

    const Stack = createNativeStackNavigator();//para navigation

    const App = () => {
      return (
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name="Iniciar Sesion" component={SignIn} />
            <Stack.Screen name="Inicio" component={Home} />
          </Stack.Navigator>
        </NavigationContainer>
      );
    }
    export default App;
    ```

=== "sign-in.js"

    ```JS
    import React from 'react'
    import { Text, View } from 'react-native'

    const SignIn = ({navigation}) => {
        return (
            <View>
                <Text onPress={() => navigation.navigate('Inicio', {name: 'Luis'})}>Inicio</Text>
            </View>
        )
    }
    export default SignIn
    ```

=== "Home.js"

    ```JS
    import React from 'react'
    import { Text, View } from 'react-native'

    const Home = ({navigation, route}) => {
        return (
            <View>
            <Text>Hola {route.params.name}</Text>
                <Text onPress={() => navigation.navigate('Iniciar Sesion')}>Iniciar Sesion</Text>
            </View>
        )
    }
    export default Home
    ```

# Forma 2 de Navegacion

```JS
import { useNavigation } from '@react-navigation/native';

const navigation = useNavigation();

navigation.navigate('Iniciar Sesion')
```
