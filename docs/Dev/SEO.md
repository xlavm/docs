# SEO

son las siglas en ingles de *Optimizacion para Motores de Busqueda*, tambien se conoce como *Posicionamiento en Buscadores*

Conoce mas en: <https://developers.google.com/search?hl=es&card=owner>

- `<H1>` solo 1 etiqueta H1 con el titulo principal del sitio.
- `<title>` 35-65 caracteres.
- `<meta name="description"  content=""/>` 50 - 160  caracteres.
- `<link rel="canonical" href="https://xlavm.gitlab.io/personalsite/index.html">` esta etiqueta le dice a los bots cual URL usar en los resultados de busquedas.
- `robots.txt` es un archivo de texto que se coloca en los sitios web para informar a los robots de los motores de búsqueda (como Google ) qué páginas de ese dominio se pueden rastrear.

## robots.txt

```TXT
User-agent: *
Allow: /
```

mas en <https://developers.google.com/search/docs/crawling-indexing/robots/create-robots-txt?hl=es>

## sites.map

crear el sites.map en <https://www.xml-sitemaps.com/>

Enviar a Google tu Sitemap XML en 3 sencillos pasos:

- Inicie sesión en Google Search Console <https://search.google.com/search-console/welcome> y seleccione su sitio web
- Haga clic en "Mapa del sitio" (en la barra de navegación de la izquierda)
- Digite la URL del mapa del sitio en el campo apropiado

## Mediciones

<https://pagespeed.web.dev/>

<https://sitechecker.pro/app/main/seo-report>

<https://suite.seotesteronline.com/seo-checker>

<https://metricspot.com/es/>

<https://www.seobility.net/es/seochecker/>

<https://gtmetrix.com/compress-components-with-gzip.html>