# GPG

Encriptar y desencriptar archivos con Linux.

1. Genero un par de llaves pública y privada

    ```BASH
    gpg --gen-key
    ```

2. Podemos ver la clave creada con

    ```BASH
    gpg -k
    ```

3. Ciframos el archivo con

    ```BASH
    gpg --encrypt -r usuarioDeLlave ejemplo.txt
    ```

    Esto creará un archivo llamado ejemplo.txt.gpg

4. Para descifrar se ejecuta el comando

    ```BASH
    gpg --decrypt ejemplo.txt.gpg
    ```
