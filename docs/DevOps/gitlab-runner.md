# Gitlab Runner

## Prerrequisitos

- Docker instalado

## Crear un runner de Gitlab

1. crea un contenedor de Gitlab Runner

    ```Bash
    docker run -d --name gitlab-runner --restart always \
    -v /srv/gitlab-runner/config:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner:v15.8.2
    ```

2. obten un token desde Gitlab

    Debes ir a `Settings > CI/CD > Runners

    ![runner1](img/runner1.png)

    ![runner2](img/runner2.png)

    ![runner3](img/runner3.png)

    Luego debes **Copiar** el token

3. registras el runner

    ```bash
    docker exec -it gitlab-runner gitlab-runner register --docker-privileged
    ```

    - Especifica `docker` como ejecutor
    - Especifica `docker:latest` como imagen

4. reinicias el runner

    ```bash
    docker restart gitlab-runner
    ```

## Comandos para Runners

- Listar

    ```bash
    docker exec -it gitlab-runner gitlab-runner list
    ```

- Iniciar

    ```bash
    docker exec -it gitlab-runner gitlab-runner start
    ```

- Detener

    ```bash
    docker exec -it gitlab-runner gitlab-runner stop
    ```

- Reiniciar

    ```bash
    docker exec -it gitlab-runner gitlab-runner restart
    ```

- Deregistrar por nombre

    ```bash
    docker exec -it gitlab-runner gitlab-runner unregister --name a9a3dd7b82bd
    ```

- Eliminar

    ```bash
    docker exec -it gitlab-runner gitlab-runner verify --delete
    ```

## Archivo de configuracion del Runner

El archivo de configuracion `config.toml` se puede acceder en el servidor donde corre el container del runner, en la siguiente ruta: `/srv/gitlab-runner/config/config.toml`.

![runner](img/runner4.png)

```bash
[[runners]]
  name = "e86668d40ec7"
  url = "https://gitlab.com"
  id = 0
  token = "glrt-XEHSSfY__makVaknHJDYuWbor"
  token_obtained_at = 0001-01-01T00:00:00Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    MaxUploadedArchiveSize = 0
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "docker:latest"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
```
