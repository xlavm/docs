# Vagrant

## Comandos

| Comando                                | Descripcion                  |
| -------------------------------------- | ---------------------------- |
| `vagrant up`                           | iniciar una VM               |
| `vagrant halt nombre_maquina`          | detener una VM               |
| `vagrant status nombre_maquina`        | comprobar estado de VM       |
| `vagrant destroy nombre_maquina`       | eliminar una VM              |
| `vagrant ssh nombre_maquina`           | iniciar sesion en una VM     |
| `vagrant box list`                     | listar todas las VM          |
| `vagrant plugin install nombre_plugin` | instalar plugins             |
| `vagrant ssh-config`                   | ver ip, port y otras configs |

NOTA: para ejecutar los comandos hay que asegurarse de estar dentro del folder de la maquina

```BASH
cd Vagrant/machine-folder
```

## Descarga e Instalación

Descargar la última versión tanto para Windows como para WSL

- **Windows**: <https://www.vagrantup.com/downloads.html>
- **WSL**: `sudo apt-get install vagrant`

A la final debe de quedar todos con las mismas versiones:

![versions](img/vagrant/versions.png)

### Version Windows y WSL no coinciden

Si la versión de Vagrant en su windows no es la misma que la de su WSL, busque la versión que desea en: <https://www.vagrantup.com/downloads.html>. Solo en: "descargue versiones anteriores de Vagrant".

Copia la URL del instalador .deb

![url](img/vagrant/url.png)

Descargar Vagrant desde WSL

```BASH
sudo wget https://releases.hashicorp.com/vagrant/2.2.5/vagrant_2.2.5_x86_64.deb
```

Instalar el Vagrant

```BASH
sudo dpkg -i vagrant_2.2.5_x86_64.deb
```

Verificar version de Vagrant

```BASH
vagrant --version
```

### Version Vagrant y VirtualBox no son compatibles

Este error sucede por ejemplo con la  versión real de Vagrant (2.2.6) no reconoce VirtualBox 6.1 como proveedor.

1. Editar

    ```BASH
    /opt/vagrant/embedded/gems/2.2.6/gems/vagrant-2.2.6/plugins/providers/virtualbox/driver/meta.rb
    ```

2. Agregue 6.1 dentro de la lista " driver_map ", como en el siguiente ejemplo

    ```BASH
    @logger.debug("Finding driver for VirtualBox version: #{@@version}")
     driver_map   = {
      "4.0" => Version_4_0,
      "4.1" => Version_4_1,
      "4.2" => Version_4_2,
      "4.3" => Version_4_3,
      "5.0" => Version_5_0,
      "5.1" => Version_5_1,
      "5.2" => Version_5_2,
      "6.0" => Version_6_0,
      "6.1" => Version_6_1,
     }
    ```

3. Crear

    ```BASH
    /opt/vagrant/embedded/gems/2.2.6/gems/vagrant-2.2.6/plugins/providers/virtualbox/driver/version_6_1.rb
    ```

    ```BASH
    require File.expand_path("../version_6_0", __FILE__)

    module VagrantPlugins
      module ProviderVirtualBox
        module Driver
          # Driver for VirtualBox 6.1.x
          class Version_6_1 < Version_6_0
            def initialize(uuid)
              super

              @logger = Log4r::Logger.new("vagrant::provider::virtualbox_6_1")
            end
          end
        end
      end
    end
    ```

4. Editar

    ```BASH
    /opt/vagrant/embedded/gems/2.2.6/gems/vagrant-2.2.6/plugins/providers/virtualbox/plugin.rb
    ```

5. Agregue 6.1 dentro de la sección " Controlador de módulo ", como en el ejemplo a continuación

    ```BASH
        # Drop some autoloads in here to optimize the performance of loading
        # our drivers only when they are needed.
        module Driver
          autoload :Meta, File.expand_path("../driver/meta", __FILE__)
          autoload :Version_4_0, File.expand_path("../driver/version_4_0", __FILE__)
          autoload :Version_4_1, File.expand_path("../driver/version_4_1", __FILE__)
          autoload :Version_4_2, File.expand_path("../driver/version_4_2", __FILE__)
          autoload :Version_4_3, File.expand_path("../driver/version_4_3", __FILE__)
          autoload :Version_5_0, File.expand_path("../driver/version_5_0", __FILE__)
          autoload :Version_5_1, File.expand_path("../driver/version_5_1", __FILE__)
          autoload :Version_5_2, File.expand_path("../driver/version_5_2", __FILE__)
          autoload :Version_6_0, File.expand_path("../driver/version_6_0", __FILE__)
          autoload :Version_6_1, File.expand_path("../driver/version_6_1", __FILE__)
        end
    ```

## Configurar Variables

Desde la ruta raíz de WSL digita el comando:

```BASH
nano .bashrc
```

Al fin del archivo agregamos las líneas:

```BASH
# Vagrant export enable
export VAGRANT_WSL_ENABLE_WINDOWS_ACCESS="1"
 
# Vagrant Provider
export PATH="$PATH:/mnt/c/Program\ Files/Oracle/VirtualBox"
```

## Usar Vagrant en Windows con WSL

Si se va a trabajar con archivos de vagrant guardados en directorios Windows, lo recomendable es usar **CMDER** <https://cmder.net/>

Se descarga y se extrae para hacer uso de el y al abrir la consola se debe usar:

![cmder](img/vagrant/cmder.png)

Ya que este es el mejor para hablar con el DrvFS Que es el sistema que comunica el sistema de archivos de windows con el de Linux del WSL.

## Crear Máquina

1. Crear una box desde bash

    ```BASH
    mkdir vagrant/machine
    ```

    ```BASH
    cd vagrant/machine
    ```

    ```BASH
    vagrant box add 'nombre_distro_o_SO'
    ```

2. Crear el Vagrantfile de la distro o SO descargada

    ```BASH
    vagrant init 'nombre_distro_o_SO'
    ```

3. Iniciar despliegue de la máquina

    ```BASH
    vagrant up 
    ```

## Vagranfile

el nombre del archivo es **vagrantfile**

Se crea una máquina de Centos 7 llamada docktos

```BASH
# -*- mode: ruby -*-
# vi: set ft=ruby :

  Vagrant.configure(2) do |config|
  
    config.vm.box = "centos/7"
  
    config.vm.define "docktos", primary: true do |h|
      h.vm.hostname = "docktos"
      h.vm.network "private_network", ip: "192.168.1.100"
      h.vm.provider "virtualbox" do |vb|
        vb.memory = "4096"
      end
    end
  end
```

Luego de tener el vagrantfile, se ejecuta el comando `vagrant up` y levantará la máquina con las especificaciones que tiene el archivo de vagrant.

## Port Forwarding Local

En la ruta Path del **vagrantfile**, ejecuta

Sintaxis

```BASH
ssh -i .vagrant\machines\centos\virtualbox\private_key vagrant@<ip_your_machine> -p <port_your_machine> -L <port_to_expose>:localhost:<port_to_expose>
```

Ejemplo

```BASH
ssh -i .vagrant\machines\jendocktos\virtualbox\private_key vagrant@127.0.0.1 -p 2222 -L 9090:localhost:9090
```

NOTA: puedes ver el `ip_your_machine` y `port_your_machine` ejecutando en la raiz del path de tu **vagrantfile** el comando

```BASH
vagrant ssh-config
```

## Network

Forma 1

```BASH
h.vm.network "private_network", ip: "192.168.1.101"
```

Forma 2

```BASH
h.vm.network "public_network", ip: "192.168.1.100", bridge: "Intel(R) Wireless-AC 9560"
```

Ejemplos

Forma 1

```BASH
config.vm.define "cendock", primary: true do |h|
      h.vm.hostname = "cendock"
      h.vm.network "private_network", ip: "192.168.1.101"
      #install docker from shell
      h.vm.provision :shell, path: "docker_install_script.sh"
      h.vm.provider "virtualbox" do |vb|
        vb.memory = "4096"
      end
    end
```

Forma 2

```BASH
config.vm.define "centdockjens", primary: true do |h|
    h.vm.hostname = "centdockjens"
    h.vm.network "public_network", ip: "192.168.1.100", bridge: "Intel(R) Wireless-AC 9560"
    h.vm.provider "virtualbox" do |vb|
      vb.memory = "4096"
    end
  end
```