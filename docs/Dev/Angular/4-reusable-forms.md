# Formularios Reusables

Esto es de utilidad cuando tenemos un mismo formulario con los mismos campos, validaciones, etc. implementado en diferentes componentes.

Uso el `SignIn` y `SignUp` como ejemplo.

Inicio creando el componente `form`

=== "form.component.ts"

    ```TS
    import { Component, Input } from '@angular/core';
    import { FormBuilder, FormGroup, Validators } from '@angular/forms';

    @Component({
      selector: 'app-form',
      templateUrl: './form.component.html'
    })
    export class FormComponent {
      @Input() formName;
      @Input() buttonName;
      @Input() linkName;
      @Input() link;
      @Input() question;
      @Input() action;
      form: FormGroup;

      constructor(private formBuilder: FormBuilder) {
        this.form = this.formBuilder.group({
          user: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]],
          pass: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(40)]],
        });
      }

      get user() { return this.form.get('user') }
      get pass() { return this.form.get('pass') }

      submit(action: any) {
        switch (action) {
          case 'signIn': this.signIn(); break;
          case 'signUp': this.signUp(); break;
        }
      }

      signIn() { alert('acabas de Iniciar Sesion') }

      signUp() { alert('acabas de Registrarte') }

    }
    ```

=== "form.component.html"

    ```HTML
    <form class="container" [formGroup]="form" (submit)="submit(action)">
        <div class="mb-3">
            <h2>{{ formName }}</h2>
        </div>
        <div class="mb-3">
            <label class="form-label">Usuario</label>
            <input type="text" class="form-control" placeholder="Usuario" formControlName="user">
            <div class="error" *ngIf="user.touched">
                <p *ngIf="user.errors?.['required']">
                    El usuario es requerido</p>
                <p *ngIf="user.errors?.['minlength']">
                    El valor tiene que ser mayor a 3 caracteres</p>
                <p *ngIf="user.errors?.['maxlength']">
                    El valor no puede ser tan grande</p>
            </div>
        </div>
        <div class="mb-3">
            <label class="form-label">Contraseña</label>
            <input type="password" class="form-control" placeholder="Contraseña" formControlName="pass">
            <div class="error" *ngIf="pass.touched">
                <p *ngIf="pass.errors?.['required']">
                    La contraseña es requerida</p>
                <p *ngIf="pass.errors?.['minlength']">
                    El valor tiene que ser mayor a 3 caracteres</p>
                <p *ngIf="pass.errors?.['maxlength']">
                    El valor no puede ser tan grande</p>
            </div>
        </div>
        <div class="button">
            <button type="submit" class="btn btn-dark" [disabled]="form.invalid">{{ buttonName }}</button>
        </div>
        <label>{{ question }} <a href="{{ link }}" class="link">{{ linkName }}</a></label>
    </form>
    ```

Luego actualizo el componente `sign-in` y  `sign-up`:

=== "sign-in.component.ts"

    ```TS
    import { Component } from "@angular/core";

    @Component({
      selector: "app-sign-in",
      templateUrl: "./sign-in.component.html",
    })
    export class SignInComponent {

    }
    ```

=== "sign-in.component.html"

    ```HTML
    <app-form 
    [formName]="'Iniciar Sesion'"
    [buttonName]="'Ingresar'"
    [linkName]="'Registrate'"
    [link]="'signup'"
    [question]="'No tienes cuenta?'"
    [action]="'signIn'">
    </app-form>
    ```

=== "sign-up.component.ts"

    ```TS
    import { Component } from "@angular/core";

    @Component({
      selector: "app-sign-up",
      templateUrl: "./sign-up.component.html",
    })
    export class SignUpComponent {

    }
    ```

=== "sign-up.component.html"

    ```HTML
    <app-form 
    [formName]="'Registrarse'"
    [buttonName]="'Registrar'"
    [linkName]="'Inicia Sesion'"
    [link]="'signin'"
    [question]="'Ya tienes cuenta?'"
    [action]="'signUp'">
    </app-form>
    ```
