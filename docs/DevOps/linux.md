# Linux

Linux es un sistema operativo semejante a Unix, de código abierto y desarrollado por una comunidad, para computadoras, servidores, mainframes, dispositivos móviles y dispositivos embebidos.

## Conceptos

- **GNU/Linux:** es un Sistema operativo libre que Podemos modificar y hacer con el lo que querramos, desde cambiar el Kernel y de más. este se encuentra bajo la licencia GPL
- **GNU GPL:** es una licencia de derecho de autor ampliamente usada en el mundo del software libre y código abierto.
- **BASH:** intérprete de comandos más común.
- **PROMT:** son los caracteres que se muestran en una línea de comandos

    Ejemplo: El promt del bash es similar a este: **luisangel@ubuntu$** lo cual equivale a esto: **usuario@hostname$**

    El signo **$** indica que se está trabajando con un usuario común

    El signo **#** indica que se está trabajando con un usuario root

- **Terminal de Línea de Comandos:** es donde damos las instrucciones a nuestros sistemas de lo que queremos que el haga.

- **Comandos**: son la instrucciones que le damos a la maquina para que ejecute

    Su sintaxis es:

    ![sintax](img/linux/sintax.png)

    **--** representa opciones por palabras

    **-** representa opciones por letras

- **Sistemas de Archivos:** Es como esta distribuido los archivos de Linux.

    ![fs](img/linux/file-system.png)

    **Directorios:**

    | Directorio     | Descripcion                                          |
    | -------------- | ---------------------------------------------------- |
    | `/boot`        | Contiene imagen del kernel e info de arranque del SO |
    | `/dev`         | Dispositivos reconocidos por el sistema              |
    | `/home`        | Directorio de los "usuarios comunes"                 |
    | `/media`       | Puntos de montajes para medios extraibles            |
    | `/root`        | Directorio home del superusuario                     |
    | `/etc`         | Directorio de archivos de configuracion              |
    | `/bin - /sbin` | Binarios ejecutables                                 |

## Comandos

### Comunes

| Comando                            | Descripcion                                        |
| ---------------------------------- | -------------------------------------------------- |
| `pwd`                              | muestra la ruta donde estas                        |
| `cd`                               | ir al inicio                                       |
| `cd ..`                            | devolverse un directorio atras                     |
| `cd nombre_directorio`             | ir a un directorio especifico                      |
| `mkdir nombre_directorio`          | crea un directorio                                 |
| `rm -rf /nombre_directorio`        | elimina el directorio recursivamente               |
| `cp -r /ruta/archivo .`            | copiar archivo a la ruta actual (contexto ".")     |
| `cp -r /mnt/d/archivo /home/user/` | copia el archivos de Windows d y lo pega en Linux  |
| `sudo`                             | ejecutar con usuario root                          |
| `sudo su`                          | cambiarse a un usuario root                        |
| `!!`                               | mostrar historial de comandos                      |
| `df -h`                            | Disk Free muestra el espacio libre (Recomendable)  |
| `more nombre_archivo`              | muestra detenidament lo que tiene dentro un archivo|
| `nano nombre_archivo`              | editar archivos de texto interactivamente          |
| `vim nombre_archivo`               | editar archivos de texto de forma old school       |
| `diff archivo1 archivo2`           | compara dos archivos                               |
| `wget http://site.com/programa.tar`| descarga un archivo de la web                      |

### Busquedas

Sintaxis básica: find <ruta_donde_buscar> <patron_seleccion>

| Comando                         | Descripcion                                                                      |
| ------------------------------- | -------------------------------------------------------------------------------- |
| `find /ruta/ -name *.txt`       | busca en esa ruta los archivos con extension txt                                 |
| `find /ruta/ -name miarchivo.*` | busca en esa ruta los archivos con nombre "miarchivo" sin importar extension     |
| `grep "texto" /home/ *.txt`     | busca la palabra “texto” dentro de los archivos .txt que están en Home           |
| `grep -r "texto" /home/ *.txt`  | hace lo mismo de arriba, pero recursivamente para los directorios dentro de Home |
| `grep -i "texto" *.*`           | busca en todos los archivos de todas las extensiones, la palabra “texto”         |
| `grep -i -r "m" ./`             | busca la letra “m” recursivamente dentro de los archivos que estén dentro de ./  |

### Archivos y Directorios

| Comando                          | Descripcion                                                             |
| -------------------------------- | ----------------------------------------------------------------------- |
| `ls`                             | lista los directorios                                                   |
| `ls -r`                          | lista recursivamente el contenido de un directorio                      |
| `ls -l`                          | lista los directorios con detalles (formato long)                       |
| `ls -lh`                         | lista los tamaños en formato legible (formato humano + long)            |
| `ls -la`                         | lista archivos ocultos                                                  |
| `ls -s`                          | lista archivos ordenados por tamanio                                    |
| `ls -t`                          | lista archivos ordenados por tiempo de modificación                     |
| `less nombre_archivo`            | muestra lo que tiene un archivo dentro, permite ir al incio del archivo |
| `cat nombre_archivo`             | muestra lo que tiene un archivo dentro, todo de una vez                 |
| `head nombre_archivo`            | muestra las primeras 10 líneas del archivo                              |
| `tail nombre_archivo`            | muestra las ultimas 10 líneas del archivo                               |
| `head nombre_archivo -n23`       | muestra las primeras 23 líneas del archivo                              |
| `tail nombre_archivo -n23`       | muestra las ultimas 23 líneas del archivo                               |
| `nl nombre_archivo`              | enumera las lineas de un archivo                                        |

### Descargas de la Web

| Comando                            | Descripcion                                                                           |
| ---------------------------------- | ------------------------------------------------------------------------------------- |
| `wget -i archivos.txt`             | descarga una serie de archivos (en el .txt debe haber 1 URL por linea)                |
| `wget -o reporte.txt site.com`     | añadir Logs a la descarga                                                             |
| `wget --limit-rate=50k site.com`   | limitar ancho de banda de descarga                                                    |
| `wget -t 5 site.com`               | dar un numero de intentos de descarga                                                 |
| `wget -r www.ejemplo.com`          | descarga web con archivos extras css, imagenes, etc.                                  |
| `wget --mirror www.ejemplo.com`    |  descarga el sitio completo (recursividad a nivel infinto y remplaza: “-r -l inf -N”) |

### Espacios en Discos

| Comando                          | Descripcion                                                             |
| -------------------------------- | ----------------------------------------------------------------------- |
| `du -sh nombrearchivo`           | Disk Usage Muestra cuantas GB está usando el archivo                    |

### Servicios y Puertos

## Convertir fin de línea Windows a Linux

Para convertir el fin de linea de todos los archivos dentro de **src**, nos pocisionamos dentro de src y ejecutamos:

```BASH
find . -type f -print0 | xargs -0 sed -i 's/\x0D$//'
```

## Reemplazar valores de archivos de un fichero X y sus subficheros de forma recursiva 

Posicionándonos dentro del fichero X, ejecutamos el comando:

```BASH
find. -type f -print0 | xargs -0 sed -i 's/<palabra_actual>/<nueva_palabra>/g'
```

## Ubuntu

### Instalar server y acceder por SSH

1. Acceso Remoto al Server

    Por medio de SSH nos conectaremos. Si el server no cuenta con el SSH, lo instalamos

    ```BASH
    sudo apt-get install openssh-server
    ```

    Para acceder desde otro dispositivo linux es necesario el ssh client que se puede instalar así

    ```BASH
    sudo apt-get install openssh-client
    ```

2. Conectar por ssh al server

    Este es el comando

    ```BASH
    ssh usuario@ip_maquina
    ```

    Para acceder desde un Puerto específico

    ```BASH
    ssh -p 9000 usuario@ip_maquina
    ```
