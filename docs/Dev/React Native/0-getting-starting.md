# Empezando

## Instalar

- NodeJS
- Java JDK 11
- Android Studio
- Android SDK
- Android SDK Tools:
    1. Build-tools
    2. Command-line-tools
    3. Emulator
    4. Emulator Hypervisor AMD o Emulator Hypervisor Intel (depende de tu procesador)
    5. Platform-tools
    6. USB Driver
- Crear AVD en Android Studio

## Ambientar

- Añadir `Android/Sdk` al PATH de windows como una nueva variable llamada "ANDROID_HOME"
- Añadir `%LOCALAPPDATA%\Android\Sdk\platform-tools` al la variable PATH
- Añadir `%LOCALAPPDATA%\Android\Sdk\tools\bin` al la variable PATH
- Añadir `%LOCALAPPDATA%\Android\Sdk\tools` al la variable PATH
- Añadir `%LOCALAPPDATA%\Android\Sdk\emulator` al la variable PATH

Verificas las instalaciones y variables de entorno

```BASH
adb --version
#Android Debug Bridge version 1.0.41
#Version 34.0.0-9570255
#Installed as C:\Users\USER\AppData\Local\Android\Sdk\platform-tools\adb.exe

emulator
#emulator: ERROR: No AVD specified. Use '@foo' or '-avd foo' to launch a virtual device named 'foo'

#verificas el AVD o emulador que creaste en Android Studio
emulator -list-avds
#Pixel_6_Pro_API_31

```

## Crear Proyecto

```BASH
npm install -g @react-native-community/cli
npx @react-native-community/cli init <nombre_proyecto>
npx react-native init <nombre_proyecto>
cd <nombre_proyecto>
npx react-native run-android
```

**Nota**: Si en el simulador no aparece la app ejecutandose, nos paramos encima y presionamos `Control + M` o presionando `m` en la consola de Metro

## Ejecutar Proyecto

Te situas dentro de la carpeta del proyecto

=== "Android"

    ```BASH
    npm i --force
    npx react-native run-android
    ```

=== "iOS"

    ```BASH
    npm i --force
    cd ios/
    pod install
    npx react-native run-ios
    ```

## Estructura del proyecto

```BASH
android/            # Estructura para Android
ios/                # Estructura para iOS
app.js              # Archivo principal
app.json            # Archivo de proyecto
index.js            # Archivo proxy
metro.config.js     # Configuracion de Metro
package.json        # Configuraciones del Proyecto
components/         # Carpeta con componentes
    header.js
    form.js         # Componente
    footer.js
assets/             # Carpeta de recursos
    img/
        logo.png    # Imagen recurso
        image.png
    fonts/
        didot.font  # Fuente recurso
```

## Archivos Principales

- app.js
- package.json
- components/
- assets/

## Estructura app.js

```JS
import React from 'react';
const App  = () => {
  return (
    <>
        /* aqui van los componentes, ej: <miComponente/> */
    </>
  );
};
export default App;
```

## Estructura component.js

```JS
import React from 'react';
const miComponente = () => (
    // aqui agrego los elementos del componente, ej: <Text style={styles.encabezado}>Criptomonedas</Text>
 );
export default miComponente;
```

## Desplegar Proyectos

=== "Android"

    - Dentro de `android/app/src/main/` crear la carpeta: `assets`

    - Abre la terminal desde la ruta de tu proyecto y ejecuta los comandos:

        ```BASH
        npm i --force
        ```

        ```BASH
        npx react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res
        ```

        ```BASH
        cd android && gradlew assembleDebug
        ```

    - Listo ya tienes tu APK generada y la puedes encontrar en el siguiente directorio de tu proyecto: `project/android/app/build/outputs/apk/debug/app-debug.apk`

=== "iOS"

    - Abre la terminal desde la ruta de tu proyecto y ejecuta los comandos:

        ```BASH
        npx react-native run-ios --configuration=release
        ```

## Comandos Metro

```BASH
r - reload the app
d - open developer menu
i - run on iOS
a - run on Android
```
