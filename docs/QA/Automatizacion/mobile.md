# Mobile

Estos proyectos de automatizacion cumplen con el patron de diseño `Page Object Model (POM)`.

Tecnologias:

- Pytest (python)
- Robotframework (python)
- WebdriverIO (javascript)

**Documentos de apoyo**

- Pytest: <>

- Robotframework:<>

- WebdriverIO:<>

## Frameworks Base

<https://gitlab.com/lavm/qaa-frameworks/-/tree/main/frameworks-bases/mobile?ref_type=heads>
