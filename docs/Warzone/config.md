# Configuracion Warzone

Muestro configuraciones en Windows y en Warzone

## Windows

### Prioridad

![prioridad](img/prioridad.png)

### Mouse

![mouse](img/mouse.png)

## Warzone

### Graficos

![graficos1](img/graficos1.png)

![graficos2](img/graficos2.png)

![graficos3](img/graficos3.png)

![graficos4](img/graficos4.png)

![graficos5](img/graficos5.png)

![graficos6](img/graficos6.png)

![graficos7](img/graficos7.png)

![graficos8](img/graficos8.png)

### mando

![mando1](img/mando1.png)

![mando2](img/mando2.png)

![mando3](img/mando3.png)

![mando4](img/mando4.png)

### Teclado y Raton

![tec-raton1](img/tec-raton1.png)

![tec-raton2](img/tec-raton2.png)

### Colores

![colores1](img/colores1.png)
