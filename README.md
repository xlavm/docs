# LUIS VANEGAS DOCS

## Ejecucion

>Prerrequisitos: `Python` y `pip` instalado

- Instale MkDocs, mkdocs-material, y el paquete de lenguajes i18n asi:

```BASH
python -m venv env && env\Scripts\activate && py -m pip install mkdocs && py -m pip install mkdocs-material && py -m pip install mkdocs[i18n]
```

- Ejecute el siguiente comando en el path del `mkdocs.yml` para abrir el sitio:

```BASH
env\Scripts\activate && py -m mkdocs serve
```

## Build

- Ejecute el siguiente comando en el path del `mkdocs.yml` para CONSTRUIR el sitio:

```BASH
env\Scripts\activate && py -m mkdocs build
```

---

## Personalizacion

Para personalizar el disenio, ingresa a: <https://squidfunk.github.io/mkdocs-material/setup/changing-the-colors/>
