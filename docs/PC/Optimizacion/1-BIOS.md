# BIOS

Para entrar a la BIOS es: `Shift + Inicio + Reiniciar` luego `Solucionar problemas`, `Opciones avanzadas`, `Configuracion de firmware UEFI` y `Reiniciar`

Si quieres ingresar reiniciando la PC se hace presionando la tecla `DEL` o `F12` o `F2` durante el inicio del sistema.

## Actualizar BIOS

Asegurate de conectar la USB al puerto USB Blanco de la placa base

<https://www.youtube.com/watch?v=wAAKtwYfIRU&ab_channel=DiegoJesusAmaya>

## Virtualization

1. Dentro de la Bios, navegue al menú `Tweaker` y `Avanced CPU Settings`
2. Habilite el `SVM Mode`

<https://www.youtube.com/watch?v=QlEL_2HylCE&ab_channel=CachoLike>

## AMD Smart Access Memory

El procesador aprovecha la memoria gráfica (VRAM) de la GPU para eliminar los cuellos de botella y aumentar el rendimiento.

1. Dentro de la Bios, navegue al menú `Settings` y `IO Ports` o `PCI`
2. Habilite o ponga Auto `Above 4G Decoding` y `Re-Size BAR Support`
3. Navegue al menú `Boot` y deshabilite `CSM Support`

<https://www.youtube.com/watch?v=5sK4iSJNt4Q>

## Extreme Memory Profile (X.M.P.) de RAM

Habilita el trabajo al maximo rendimiento de la memoria RAM

1. Dentro de la Bios, navegue al menú `Tweaker` y `Extreme Memoru Profile(X.M.P.)`
2. Selecciona el perfil mas alto `Profile1`, `Profile2`, (Si hay 3 perfiles, usar el perfil3) etc.

![xmp](img/XMP.jpg)

## Presicion Boost Overdrive

Aumenta automaticamente las frecuencias de trabajo del PC

1. Dentro de la Bios, navegue al menú `Tweaker` y `Precision Boost Overdrive`
2. Habilite `Presicion Boost Overdrive`

![precision-boost](img/precision-boost.jpg)

## DDR and Infinity Fabric Frecuency

1. Dentro de la Bios, navegue al menú `Settings`, `AMD Overclocking` y `DDR and Infinity Fabric Frecuency/Timings`
2. En `Infinity Fabric Frecuency and Dividers` se selecciona el valor de la mitad de la velocidad (MHz) de la memoria RAM que hemos visto en el X.M.P. (3600/2=1800)

![infinity-fabric](img/infinity-fabric.jpg)


