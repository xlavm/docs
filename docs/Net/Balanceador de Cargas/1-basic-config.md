# Configuracion Basica

Prerrequisitos:

- 2 ISP
- Load balancer
- Access point (AP)
- Cables UTP
- Computador

## Herramientas

- Fing <https://www.fing.com/>: Esta herramienta ayuda a administrar la red, hacer pruebas de velocidad y ver dispositivos conectados en tiempo real, entre otros.

## Configurar Load Balancer

Conecto un extremo de 1 cable UTP al puerto LAN del Load Balancer, y el otro extremo al puerto Ethernet del computador

![conex-fisica](img/conex-fisica.jpg)

Valido la puerta de enlace del Load Balancer

![puerta-enlace](img/console.png)

Creo el usuario y la contrasenia para el Load Balancer

![user](img/userlb.png)

Habilito 2 WAN para mis 2 ISP

![wan](img/wan.png)

Configuro la WAN1

![wan1](img/wan1.png)

Configuro la WAN2

![wan2](img/wan2.png)

Aqui ya puedo conectar los dos cables WAN y hacer uso del Load Balancer desde el computador

![conex-fisica-final](img/conex-fisica-final.jpg)

## Configurar Access Point

Esto es para las conexiones inalambricas, Uso un router y lo configuro en modo AP.

Inicio conetando el PC al Router AP por medio de cable UTP.

![conex-fisica-ap](img/conex-fisica-ap.jpg)

Valido la puerta de enlace del Router AP

![puerta-enlace-ap](img/console-ap.png)

*Note que la puerta de enlace es la misma que la del Load Balancer* HAY QUE CAMBIAR LA PUERTA DE ENLACER DEL LOAD BALANCER.

Inicio sesion en el Router AP

![ap-login](img/aplogin.png)

Configuro el modo AP

![ap-config](img/apconfig.png)

![ap-mode](img/apmode.png)

Luego hay que desconectar el cable que va del PC al Router AP

## Cambiar puerta de enlace del Load Balancer

Conecto el computador al load balancer por cable UTP e ingreso a su sitio de administracion y luego a LAN y cambio la IP de 192.168.0.1 a 192.168.2.1

![change-ip-lan-lb](img/changeiplanLB.png)

Hay que esperar que se reinicie el Load Balancer

Por ultimo se conecta el Router AP al Load Balancer por medio de LAN a LAN con cable UTP y aqui se conecta el computador via Wifi.

![conex-fisica-lb-ap](img/conex-fisica-lb-ap.jpg)

Estas son las direcciones para acceder a la administracion:

- AP: <http://www.nexxtwifi.local/login.html?1662863759>
- LOAD BALANCER: <http://192.168.2.1/webpages/login.html>

## Configuraciones Adicionales

Configuro los `DNS` para todas las WAN en el Network

![dns1](img/dns1.png)

Configuro los `DNS` para todas las WAN en el Online Detection del Transmission

![dns2](img/dns2.png)

HASTA ESTE PUNTO DEBE DE HABER INTERNET Y TODO DEBIDAMENTE FUNCIONANDO
