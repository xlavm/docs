# POO

Es un **paradigma de programación**, así como los siguientes paradigmas: programación imperativa, declarativa, funcional, etc.

## Algunos Conceptos

- **Entidades:** mouse, lápiz, teclado, ingeniero son entidades y a la vez objetos.
- **Objeto:** tienen características y funciones o acciones. Son entidades que tienen características que los hacen diferentes a otros y cumplen diferentes acciones.
- **Clase:** es un molde con el cual nosotros podemos crear diferentes objetos
Un objeto es una instancia de una clase.

## Encapsulación, acoplamiento y cohesión

### Encapsulamiento

Es **ocultar los estados, atributos o propiedades internas de un objeto**. Son necesarios para realizar una interacción con un método dentro del objeto,
a esto se le conoce como “encapsulación de datos”.
Esto permite aumentar la **cohesión** de los componentes del sistema.

### Acoplamiento

Es **el nivel de dependencia de una clase con otra**. Cuanto más necesite saber una clase como trabaja otra, entonces tendrán un mayor acoplamiento.

### Cohesión

Esto es Que **cada clase sea responsable de una tarea bien definida**.
Cuanto más enfoquemos el propósito de la clase, mayor será su cohesión.

**Recuerda siempre:** Alta cohesión y bajo acoplamiento.

## Herencia, sobrecarga y sobreescritura

### Herencia

La herencia permite que las clases **hereden el estado y el comportamiento** de la aplicación general de otras clases.

**Ejemplo:** tenemos 2 clases, una llamada Padre y otra llamada hijo, realizamos los métodos del Padre:

- camina()
- come()
- trabaja()
- getNombre()

=== "JAVA"

    ```JAVA
    public class Principal{
        public static void main(String[] args) {
            Hijo miHijo = new Hijo();
            miHijo.camina();
        }
    }

    public class Padre{
        private String nombre;
            
        public void Padre(String nombre){
                this.nombre=nombre;
        }
        public void camina(){
            System.out.println("el padre camina");
        }
        protected void come(){
            System.out.println("el padre come");
        }
        private void trabaja(){
            System.out.println("el padre trabaja");
        }
        public String getNombre(){
            return this.nombre;
        }
    }

    public class Hijo extends Padre{
        public Hijo(){
            super();
        }
    }
    ```

    La clase hijo hereda de padre y esta herencia se trata con la palabra extends 
    un hijo hace lo mismo que padre.

    protected significa que solo será visible por las clases que son heredadas y que estén dentro del paquete

=== "JAVASCRIPT"

    ```JS
    class Padre{
        nombre = 'Luis'
        constructor(){
            console.log('soy un padre')
        }
        camina = () =>{
            console.log("el padre camina");
        }
        come = () =>{
            console.log("el padre come");
        }
        trabaja = () =>{
            console.log("el padre trabaja");
        }
        getNombre = () =>{
            return this.nombre;
        }
    }

    class Hijo extends Padre{
        constructor(){
            super()
        }
    }

    const miHijo = new Hijo()//output: soy un padre
    miHijo.camina()//output: el padre camina
    miHijo.getNombre()//output: 'Luis'
    ```

    La clase hijo hereda de padre y esta herencia se trata con la palabra extends 
    un hijo hace lo mismo que padre.

=== "PYTHON"

    ```PY
    class Padre():
        def __init__(self, nombre):
            self.nombre = nombre
        def caminar(self):
            print("El padre camina")
        def comer(self):
            print("El padre come")
        def trabajar(self):
            print("El padre trabaja")
        def getNombre(self):
            return self.nombre
        def describeme(self):
            print("Soy un Padre del tipo", type(self).__name__)

    class Hijo(Padre):
        pass

    miHijo = Hijo('Luis')
    miHijo.describeme()
    ```

    La clase hijo hereda de padre y esta herencia se trata asi: Hijo(Padre)

    La instruccion `pass` es para darle continuidad al blucle o ciclo.

### Sobrecarga

se puede declarar 2 métodos con el mismo nombre, pero con distintos parámetros.

**Ejemplo:**

=== "JAVA"

    ```JAVA
    public class Principal{
        public static void main(String[] args) {
            Hijo miHijo = new Hijo();
            miHijo.camina(1.0);
        }
    }

    public class Hijo{
        public void camina(int tiempoRecorrido){
            System.out.println("el Hijo camina durante: "+tiempoRecorrido+" segundos");
        }
        public void camina(double metrosDistancia){
            System.out.println("el Hijo camina: "+metrosDistancia+ " metros de distancia");
        }
    }
    ```

    al invocar el método camina, con un parámetro entero, este escogerá el método que concuerde con el tipo de parámetro.

    sí llamo camina(2), me llamará al método que tiene tipo de dato entero;

    sí llamo al método camina(1.0), me llamará al método que tiene tipo de dato decimal.

=== "JAVASCRIPT"

    *Nativamente NO es posible.*

=== "PYTHON"

    *Nativamente NO es posible.*

### Sobreescritura

**Se podrá cambiar la forma en la que se comportan los métodos de la clase padre**.

**Ejemplo:**

=== "JAVA"

    Anteponiendo la palabra `@Override` antes del método, este sobrescribirá el método de la clase padre, para ello el método debe estar igual al método de la clase padre.

    ```JAVA
    public class Principal{
        public static void main(String[] args) {
            Hijo miHijo = new Hijo();
            miHijo.camina();
        }
    }

    public class Padre{
        private String nombre;
            
        public void Padre(String nombre){
                this.nombre=nombre;
        }
        public void camina(){
            System.out.println("el padre camina");
        }
        protected void come(){
            System.out.println("el padre come");
        }
        private void trabaja(){
            System.out.println("el padre trabaja");
        }
        public String getNombre(){
            return this.nombre;
        }
    }

    public class Hijo extends Padre{
        public Hijo(){
            super();
        }

        @Override
        public void camina(){
            System.out.println("el hijo camina");
        }
    }
    //output: el hijo camina
    ```

    Si quiero sobrescribir el método de la clase Padre pero aparte quiero que adicionalmente se ejecute lo que está en el método camina() de la clase Hijo, solo basta con hacer lo siguiente en la clase Hijo:

    ```JAVA
    @Override
    public void camina(){
        super.camina();//hace referencia a la clase Padre a traves de la palabra super
        System.out.pintln("el hijo camina")
    }
    ```

=== "JAVASCRIPT"

    Creando el metodo camina() en la clase hijo, este sobrescribirá el método de la clase padre sin necesidad de usar un tag especial.

    ```JS
    class Padre{
        nombre = 'Luis'
        constructor(nombre){
           this.nombre = nombre
        }
        camina = () =>{
            console.log('el padre camina')
        }
    }

    class Hijo extends Padre{
        constructor(){
            super()
        }
        camina = () => {
            console.log('el hijo camina')
        }
    }

    const miHijo = new Hijo()
    console.log(miHijo.camina())//output: el hijo camina
    ```

=== "PYTHON"

    Puedo usar `super()` para sobreescribir un metodo de la clase Padre desde la clase Hijo, en este caso el metodo __init__ agregandole desde hijo el apellido

    Tambien puedo sobreescribir un metodo del padre creando el mismo metodo con el mismo nombre en la clase hijo asi:

    ```PY
    class Padre():
        def __init__(self, nombre):
            self.nombre = nombre
        
        def camina(self):
            print('el padre camina')

    class Hijo(Padre):
        def __init__(self, nombre, apellido):
            super().__init__(nombre)
            self.apellido = apellido
        
        def camina(self):
            print('el hijo camina')

    miHijo = Hijo('Luis', 'Vanegas')
    print(miHijo.apellido)#output: Vanegas
    miHijo.camina()#output: el hijo camina
    ```
    
    Si quisiera usar el metodo camina() del Padre y del Hijo lo haria con la palabra super() asi:

    ```PY
    class Hijo(Padre):
        def __init__(self, nombre, apellido):
            super().__init__(nombre)
            self.apellido = apellido
        
        def camina(self):
            super().camina()
            print('el hijo camina')

    miHijo = Hijo('Luis', 'Vanegas')
    miHijo.camina()
    #output: el padre camina
    #output: el hijo camina
    ```

## Polimorfismo

Es la capacidad de un objeto en tomar muchas formas.

**Ejemplo:**

Creo un objeto Animal y le doy la forma de Perro

=== "JAVA"

    ```JAVA
    public class Principal{
        public static void main(String[] args) {
            Animal miAnimal = new Perro(); //aquí le decimos que toma forma de un perro
            miAnimal.dice();
        }
    }

    public class Animal{
        public void dice(){
            System.out.println("que ruido puedo hacer?");
        }
    }

    public class Perro extends Animal{
        @Override
        public void dice(){
            System.out.println("Guau!!");
        }
    }

    public class Gato extends Animal{
        @Override
        public void dice(){
            System.out.println("miau!!");
        }
    }
    //output: Guau!!
    ```

=== "JAVASCRIPT"

    ```JS
    class Animal {
        dice = () => {
            console.log("que ruido puedo hacer?")
        }
    }

    class Perro extends Animal {
        dice = () => {
            console.log("Guau!!")
        }
    }

    class Gato extends Animal {
        dice = () => {
            console.log("miau!!")
        }
    }

    let miAnimal = new Animal() //aquí le decimos que toma forma de un animal
    miAnimal.dice()//output: que ruido puedo hacer?
    miAnimal = new Perro() //aquí le decimos que toma forma de un perro
    miAnimal.dice()//output: Guau!!
    ```

=== "PYTHON"

    ```PY
    class Animal:
        def dice(self):
            print("que ruido puedo hacer?")

    class Perro(Animal):
        def dice(self):
            print("Guau!!")

    class Gato(Animal):
        def dice(self):
            print("miau!!")

    miAnimal = Animal()
    miAnimal = Perro()
    miAnimal.dice()
    #output: Guau!!
    ```

## Interfaces

No es una clase, es una forma o una guía que obliga a objetos de diferentes tipos a implementar ciertos métodos.

**Ejemplo:**

=== "JAVA"

    En Java se distingue una clase implementa una interfaz, porque lleva la palabra `implements`

    ```JAVA
    public class Principal{
        public static void main(String[] args) {
            Carro miCarro = new Carro();
            miCarro.avanza();
        }
    }

    public interface Rueda{
        public abstract void avanza();
        public abstract void detiene();
    }

    public class Carro implements Rueda{
        public void avanza(){
            System.out.println("el carro está avanzando");
        }
        public void detiene(){}
    }

    public class Bicicleta implements Rueda{
        public void avanza(){
            System.out.println("la bicicleta está avanzando");
        }
        public void detiene(){}
    }
    //output: el carro está avanzando
    ```

    Con lo anterior decimos que Carro y Bicicleta implementa una interfaz que se llama Rueda

    El objeto Carro y Bicicleta tiene que implementar todas las funcionalidades de la interface Rueda

=== "JAVASCRIPT"

    *Nativamente NO es posible.*

=== "PYTHON"

    *Nativamente NO es posible.*

**NOTA:** Es parecido al polimorfismo, pero son dos objetos totalmente distintos, incluso no heredan entre
ambos, no son ni hijos ni padres o primos (no tienen relación directa).

### Implementacion de 2 Interfaces

=== "JAVA"

    Puedo implementar 2 interfaces, Rueda y Luces en una misma clase Carro:

    ```JAVA
    public class Principal{
        public static void main(String[] args) {
            Carro miCarro = new Carro();
            miCarro.avanza();
            miCarro.enciendeLuces();
        }
    }

    public interface Rueda{
        public abstract void avanza();
        public abstract void detiene();
    }

    public interface Luces{
        public abstract void enciendeLuces();
    }

    public class Carro implements Rueda, Luces{
        public void avanza(){
            System.out.println("el carro está avanzando");
        }
        public void enciendeLuces(){
            System.out.println("el carro está encendiendo las luces");
        }
        public void detiene(){}
    }
    //output: el carro está avanzando
    // el carro está encendiendo las luces
    ```

=== "JAVASCRIPT"

    *Nativamente NO es posible.*

=== "PYTHON"

    *Nativamente NO es posible.*

## Abstracción o Clases abstractas

la diferencia de una clase abstracta y una convencional es que **esta no se puede instanciar**.

Por lo general la clase abstracta **es una clase padre que a su vez es heredada**.

**Ejemplo:**

=== "JAVA"

    ```JAVA
        public class Principal{
        public static void main(String[] args) {
            Gato gato = new Gato();
            gato.Alimentarse();
            Animal perro = new Perro();
            perro.Alimentarse();
        }
    }

    public abstract class Animal {
        // clase abstracta
        public abstract void Alimentarse(); //metodo abstracto, vacio
    }

    public  class Gato extends Animal{
        public void Alimentarse(){
            System.out.println("El Gato come croquetas");
        }
    }

    public  class Perro extends Animal{
        public void Alimentarse(){
            System.out.println("El Perro come carne");
        }
    }
    //output: El Gato come croquetas
    //El Perro come carne
    ```

    las clases que heredan la clase animal, **obligatoriamente tienen que implementar los métodos abstractos de la clase abstracta.**

=== "JAVASCRIPT"

    *Nativamente NO es posible.*

=== "PYTHON"

    *Nativamente NO es posible.*
