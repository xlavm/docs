# Terraform

Usare el proveedor AWS

## Prerrequisitos

- AWS

```BASH
sudo apt-get install awscli
aws --version
```

Tambien puedes consultar mas aqui: <https://docs.aws.amazon.com/es_es/cli/latest/userguide/getting-started-install.html>

## Instalar

```BASH
# update packages 
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common curl

# add the HashiCorp GPG key.
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -

# add the official HashiCorp Linux repository.
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

# update to add the repository, and install the Terraform CLI.
sudo apt-get update && sudo apt-get install terraform

# verificate the installation
terraform -help
```

Tambien puedes consultar mas aqui: <https://developer.hashicorp.com/terraform/downloads>

## Actualizar

Si usas una version antigua o menor a 4.0

1. Descarga el archivo de instalación de Terraform <https://releases.hashicorp.com/terraform/> para tu máquina (p. ej., terraform_0.12.3_linux_amd64.zip , terraform_0.12.3_linux_arm.zip o terraform_0.12.3_linux_386.zip)

2. Instala `unzip`, si no existe ya. Por ejemplo:

    - si utilizas `Debian/Ubuntu`:

    ```BASH
    sudo apt-get install unzip
    ```

    - si utilizas `CentOS/Redhat`:

    ```BASH
    sudo yum install unzip
    ```

3. Descomprime el paquete de instalación. Por ejemplo: `unzip terraform_0.12.3_linux_amd64.zip`

4. Establece la ruta de Terraform: `sudo mv terraform /usr/local/bin/`

5. Verfica con `terraform --help`

## Ejecutar

```BASH
terraform init
terraform apply -auto-approve
```

## Comandos

| Comando                                | Descripcion                  |
| -------------------------------------- | ---------------------------- |
| `terraform init`                       | Instalar dependencias        |
| `terraform fmt`                        | formatear el script          |
| `terraform validate`                   | validar el formato           |
| `terraform plan`                       | Ver que se va a implementar  |
| `terraform apply -auto-approve`        | Crear infraestructura        |
| `terraform show`                       | Ver estado de ejecucion      |
| `terraform state list`                 | Ver recursos del script      |
| `terraform destroy -auto-approve`      | Destruir infraestructura     |

Tomado de: <https://learn.hashicorp.com/collections/terraform/aws-get-started>
