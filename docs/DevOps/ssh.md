# SSH

Es el nombre del protocolo y del programa que lo implementa. Es un interprete de ordenes seguro o Secure Shell y su función es el acceso remoto a un servidor por medio de un canal seguro en el que toda la información está cifrada.

## Crear llave RSA

En criptografía, RSA es un sistema criptográfico de clave pública desarrollado en 1979, que utiliza factorización de números enteros. Es el primer y más utilizado algoritmo de este tipo y es válido tanto para cifrar como para firmar digitalmente.

1. abre tu terminal e introduce el comando

    ```bash
    ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
    ```

2. Ponemos la ruta por defecto `~/.ssh`.

3. Ingresamos 2 veces nuestra contraseña (la de nuestra llave RSA).

4. Agrega tu clave al ssh-agent

    ```bash
    eval "$(ssh-agent -s)"
    ```

    ```bash
    ssh-add ~/.ssh/id_rsa
    ```

5. En caso que lo requiera, se asignan permisos a los archivos

    ```bash
    chmod 700 ~/.ssh/id_rsa
    ```

    ```bash
    chmod 700 ~/.ssh/id_rsa.pub
    ```

## Crear llave ED25519

Es un sistema de clave pública basados en la curva elíptica que se utilizan habitualmente para la autenticación SSH. Ofrecen mejor seguridad y desempeño que el tipo de clave RSA tradicional.

1. abre tu terminal e introduce el comando

    ```bash
    ssh-keygen -t ed25519 -C "your_email@example.com"
    ```

2. Ponemos la ruta por defecto `~/.ssh`.

3. Ingresamos 2 veces nuestra contraseña (la de nuestra llave ED25519).

4. Agrega tu clave al ssh-agent

    ```bash
    eval "$(ssh-agent -s)"
    ```

    ```bash
    ssh-add ~/.ssh/id_ed25519
    ```

5. En caso que lo requiera, se asignan permisos a los archivos

    ```bash
    chmod 700 ~/.ssh/id_ed25519
    ```

    ```bash
    chmod 700 ~/.ssh/id_ed25519

*En este punto ya puedes usar tu `.pub` en la herramienta donde deseas usar tu ssh.*
