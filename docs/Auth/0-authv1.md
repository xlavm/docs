# Auth V1

Este repositorio contiene 3 proyectos los cuales realizan las funcionalidades de `Registro de usuario (Sign up)`, `Inicio de Sesion (Sign in)` y `Obtencion de informacion (getData)`

- API Auth (`Node JS`)
- APP Auth (`React Native`)
- WEB Auth (`Angular`)

---

## Arquitectura V1

![k1](imgv1/k1.png)

---

## Mockups

<details><summary>Form - Formulario</summary>

Los valores de los labels, placeholder de los input, buttons y link son parametrizados y de acuerdo a la vista que se invoque, sera mostrado su valor, ejemplo:

- para `sign-in` el titulo seria `Iniciar Sesion`
- para `sign-up` el titulo seria `Registrarse`

Este componente form sera renderizado en cada uno de los componentes correspondientes, `sign-in` y `sign-up`.
<img src="../imgv1/form.png">
</details>

<details><summary>Sign-up - Vista</summary>

<img src="../imgv1/sign-up.png">
</details>

<details><summary>Sign-in - Vista</summary>
<img src="../imgv1/sign-in.png">
</details>

<details><summary>Home - Vista</summary>
<img src="../imgv1/home.png">
</details>

---

## Endpoints

<details><summary>healtcheck</summary>

<table>
<tr><th>Endpoint</th><th>Method</th></tr>
<tr><td>/api/auth/</td><td>GET</td></tr>
</table>

Sample Response

```json
{
    "apiname": "API-Auth",
    "developer": "Luis Vanegas",
    "message": "OK",
    "environment": "dev",
    "uptime": 256.0486486,
    "timestamp": "Tue Mar 14 2023 08:35:09 GMT-0500 (hora estándar de Colombia)"
}
```

</details>

<details><summary>signup</summary>

<table>
<tr><th>Endpoint</th><th>Method</th></tr>
<tr><td>/api/auth/signup/</td><td>POST</td></tr>
</table>

<strong>Body</strong> urlencoded

<table>
<tr><th>Key</th><th>Value</th></tr>
<tr><td>user</td><td>xlavm</td></tr>
<tr><td>pass</td><td>xlavm</td></tr>
</table>

Sample Response

```json
{
    "status": 200,
    "message": "El Usuario se creo correctamente"
}
```

</details>

<details><summary>signin</summary>

<table>
<tr><th>Endpoint</th><th>Method</th></tr>
<tr><td>/api/auth/signin/</td><td>POST</td></tr>
</table>

<strong>Body</strong> urlencoded

<table>
<tr><th>Key</th><th>Value</th></tr>
<tr><td>user</td><td>xlavm</td></tr>
<tr><td>pass</td><td>xlavm</td></tr>
</table>

Sample Response

```json
{
    "status": 200,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ4bGF2bSIsIm5hbWUiOiJ4bGF2bSQyYiQxMCR5VU85dGZvRFBQY2RFcXlUSWU3YjVPZWF3Y2REbGJxVUZJSXBKTzlPUlhCYkt4MllzMHB5NiIsImV4cCI6MTY3ODgwMzk3NTEyNiwiaWF0IjoxNjc4ODAyMTc1fQ.Jk4SQRKsOlgOIDObK6zWCad-I5z7Eqje9mh0PoXvXMM",
    "message": "Token enviado correctamente"
}
```

</details>

<details> <summary>data</summary>

<table>
<tr><th>Endpoint</th><th>Method</th></tr>
<tr><td>/api/auth/data/:user</td><td>GET</td></tr>
</table>

<strong>Authorization</strong> Bearer Token

<table>
<tr><th>Key</th><th>Value</th></tr>
<tr><td>Token</td><td>eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ4bGF2bSIsIm5hbWUiOiJ4bGF2bSQyYiQxMCR5VU85dGZvRFBQY2RFcXlUSWU3YjVPZWF3Y2REbGJxVUZJSXBKTzlPUlhCYkt4MllzMHB5NiIsImV4cCI6MTY3ODgwMzk3NTEyNiwiaWF0IjoxNjc4ODAyMTc1fQ.Jk4SQRKsOlgOIDObK6zWCad-I5z7Eqje9mh0PoXvXMM</td></tr>
</table>

Sample Response

```json
{
    "status": 200,
    "data": {
        "_id": "63efb9ca499cc723c0265081",
        "user": "xlavm",
        "pass": "$2b$10$yUO9tfoDPPcdEqyTIe7b5OeawcdDlbqUFIIpJO9ORXBbKx2Ys0py6",
        "__v": 0
    },
    "message": "Usuario encontrado correctamente"
}
```

</details>

## Tests

<details> <summary>Pruebas manuales de API con Postman</summary>

Importa el script  <a href="#">api-auth/test/api-auth.postman_collection.json</a> en Postman y ejecuta las pruebas para todos los endpoints (antes de obtener data o iniciar sesion, debes primero registrar y luego iniciar sesion).
</details>

<details> <summary>Pruebas unitarias de API con Jest</summary>

Se logra una cobertura de pruebas de hasta un <strong>97%</strong> con resultados exitosos.

<img src="../imgv1/unit-test-api.png">
</details>

<details> <summary>Pruebas E2E automatizadas con Cypress</summary>

Se crean pruebas E2E automatizadas para WEB-Auth con Cypress.

<img src="../imgv1/e2e-test-web.png">
</details>
