# Principiante

## Automatizacion Basica

Los siguientes scripts son basicos y no cumplen un patron de diseño especifico.

=== "Selenium Python"

    Crea los siguientes archivos

    === "browser-up.py"

        Levanta el navegador con una espera de 5 segundos

        ```PY
        from webdriver_manager.chrome import ChromeDriverManager
        from selenium import webdriver
        import time

        # Levanta el navegador de chrome
        driver = webdriver.Chrome(ChromeDriverManager().install())

        time.sleep(5)

        driver.quit()
        ```

    === "google.py"

        Levanta el navegador de chrome y accede a Google.com

        ```PY
        from webdriver_manager.chrome import ChromeDriverManager
        from selenium import webdriver
        import time

        # Levanta el navegador de chrome y accede a Google.com
        driver = webdriver.Chrome(ChromeDriverManager().install())
        driver.get("https://www.google.com/")

        time.sleep(5)

        driver.quit()
        ```

    === "google-2.py"

        Levanta el navegador de chrome y accede a Google.com y obtiene el VALOR del pais que aparece en la pagina principal

        ```PY
        from selenium import webdriver
        from webdriver_manager.chrome import ChromeDriverManager
        from selenium.webdriver.common.by import By
        import time

        # Levanta el navegador de chrome y accede a Google.com y obtiene el VALOR del pais que aparece en la pagina principal
        driver = webdriver.Chrome(ChromeDriverManager().install())
        driver.get("https://www.google.com/")

        paisObtenido = driver.find_element(By.CLASS_NAME, 'uU7dJb').text

        time.sleep(5)

        print('El Pais que aparece en la pagina principal es: '+ paisObtenido)

        driver.quit()
        ```

=== "Robot Framework"

    Crea el archivo `login_test.robot` con el siguiente codigo

    ```ROBOT
    *** Comments ***
    # En los *** Settings *** declaramos las librerias que vamos a usar
    # Las *** Variables *** las puedo usar en todo el archivo como si fueran de forma global
    # Los *** Test Cases *** son creados usando los Keywords, en este caso tienen argumentos
    # En los *** Keywords *** hacemos uso de los keywords de SeleniumLibrary y aqui podemos definir argumentos
    # que pueden ser usados desde los Test Cases


    *** Settings ***
    Library     SeleniumLibrary


    *** Variables ***
    ${URL_SITE}         https://test-toh.luisvanegas.com/
    ${BROWSER}          Chrome
    ${TITTLE_SITE}      TOHFront


    *** Test Cases ***
    Caso 001 verificar que el heroe 2 del dashboard sea Correcto
        Abro el navegador en la pagina de dashboard
        Veo que contenga el heroe valido    luisman

    Caso 002 verificar que el heroe 2 del dashboard sea Incorrecto
        Abro el navegador en la pagina de dashboard
        Veo que contenga un heroe invalido    luisman_invalido


    *** Keywords ***
    Abro el navegador en la pagina de dashboard
        Open Browser    ${URL_SITE}    ${BROWSER}
        Title Should Be    ${TITTLE_SITE}

    Veo que contenga el heroe valido
        [Arguments]    ${heroeValido}
        Wait Until Element Contains    xpath=/html/body/app-root/app-dashboards/div/div[2]/div/h4    ${heroeValido}

    Veo que contenga un heroe invalido
        [Arguments]    ${heroeInvalido}
        Wait Until Element Does Not Contain
        ...    xpath=/html/body/app-root/app-dashboards/div/div[2]/div/h4
        ...    ${heroeInvalido}

    ```

    Anteriormente se creo un test suite con 2 casos de pruebas

## Xpaths

XPath (XML Path Language) es un lenguaje que permite construir expresiones que recorren y procesan un documento XML. La idea es parecida a las expresiones regulares para seleccionar partes de un texto sin atributos (plain text).

### Sintaxis

| Expresion       | Descripcion                                                  |
| --------------- | ------------------------------------------------------------ |
| `/`             | seleccionar desde el nodo root                               |
| `//`            | seleccionar todos los nodos sin importar donde se encuentren |
| `.`             | seleccionar el nodo actual                                   |
| `..`            | seleccionar un nodo anterior                                 |
| `@`             | selecciona atributos                                         |
| `*`             | selecciona cualquier elemento                                |

Ejemplos:

- `//title[@lang='en']` Selecciona todos lo elementos `title` que tengan el artibuto `lang` igual a `en`.

- `//*[@lang='en']` Selecciona cualquier elemento que tenga el atributo `lang` igual a `en`.

### Avanzado

| Expresion                              | Descripcion                         |
| -------------------------------------- | ----------------------------------- |
| `//*[@text="texto1" or @text="texto2"]`| operador logico                     |
| `//*[contains(@text, "texto")]`        | funcion contains()                  |
| `//*[@text="{variable}"]`              | manejo de variables                 |

## Patrones de diseños

Los dos patrones de diseño de codigo mas conocidos son los siguientes, uno de los dos cumple con los principios S.O.L.I.D. y uno de ellos se usa mas en agilidad.

![dp](../img/design-patterns/design-patterns.png)

## Fluent Wait

Sobre los waits: La forma apropiada para hacer esperas es usar `Fluent Wait` y no Time de python, por ejemplo:

```PY
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# Realizar una consulta en Google y validar el valor del primer resultado, al final debe mostrar si el valor es correcto o no de acuerdo al resultado esperado
driver = webdriver.Chrome(ChromeDriverManager().install())
wait = WebDriverWait(driver, timeout=10, poll_frequency=1)

driver.get("https://www.google.com/")

wait.until(EC.element_to_be_clickable((By.NAME, 'q'))).send_keys('Luis Angel Vanegas Martinez')
wait.until(EC.element_to_be_clickable((By.NAME,'btnK'))).click()
resultadoObtenido = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="rso"]/div[1]/div/div/div[1]/div/a/h3'))).text

print('Valor del resultado obtenido: '+ resultadoObtenido)

driver.quit()
```
