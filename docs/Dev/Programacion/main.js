interface Rueda{
    avanza()
    detiene()
}

class Carro implements Rueda{
    avanza(){
        console.log("el carro está avanzando")
    }
    detiene(){}
}

class Bicicleta implements Rueda{
    avanza(){
        console.log("la bicicleta está avanzando")
    }
    detiene(){}
}
//output: el carro está avanzando

