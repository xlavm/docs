# Manifiesto

NO hago **Overclocking**, solo hago algunas configuraciones que nos permiten aprovechar al maximo los recursos de nuestra PC; como dejar de limitar el consumo de energia, priorizar la GPU, Deshabilitar limites de procesamiento, etc.

## Recursos

- CPU: AMD Ryzen 5600X 6-Core Proccesor
- GPU: AMD Radeon RX 6600XT 8GB
- RAM: Corsair Vengeance PRO SL 32GB DDR4 a 3200MHz, 1.35V
- DISCO: SSD Kingston SA400S 960GB
- PLACA: X570 AORUS Elite
- FUENTE: EVGA 700 GD, 80+ GOLD 700W
- MONITOR: 24" 1080P, 16:9, DPx1, 144Hz, 1ms, ADM FreeSync
- REFRIGERACION: AMD Default por disipador

## Resultados de rendimientos

Los recursos fueron medidos ejecutando Warzone 2.0 Temporada 3

- Antes de optimizar:
    1. GPU: 60% Utilizacion a 57*C
    2. CPU: 50% Utilizacion a 88*C
    3. RAM: 2133MHz Velocidad
    4. FPS: 97 promedio
    5. Respuesta: 12ms
    6. Rendimiento: Optimo

- Despues de optimizar:
    1. GPU: 70% Utilizacion a 61*C
    2. CPU: 40% Utilizacion a 82*C
    3. RAM: 3600MHz Velocidad
    4. FPS: 125 promedio
    5. Respuesta: 9ms
    6. Rendimiento: Excelente

`Estos resultados fueron medidos por AMD Adrenalin Edition`

## Prerrequisitos para Optimizar

- Windows actualizado (Windows Update) No descarges las actualizacione opcionales
- AMD Chipset Driver
- BIOS
- Drivers actualizados (AMD)
- AMD Adrenaline Edition actualizado
