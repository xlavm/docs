# Docker

Docker es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro de contenedores de software, proporcionando una capa adicional de abstracción y automatización de virtualización de aplicaciones en múltiples sistemas operativos.​

<https://docs.docker.com/>

## Conceptos

- **Filosofía de Docker:** en cada contenedor hay un servicio, en cada contenedor hay una aplicación, en cada contenedor hay solo un proceso.
- **Docker:** herramienta que crea contenedores de servicios.
- **Imagen de Docker:** es una plantilla que viene con toda la configuración del servicio que queremos (dependencias, herramientas, etc.).
- **Dockerfile:** define la estructura de una imagen. En este se ponen instrucciones para crear imágenes de Docker.
- **Contenedor de Docker:** es una instancia ejecutable de una imagen.
- **Docker-Compose:** define los servicios que componen su aplicación en docker-compose.yml para que puedan ejecutarse juntos en un entorno aislado.
- **Label:** Nos permite agregar metadatas a las imágenes de docker en un Dockerfile.

## Comandos

### Comunes

| Comando                                | Descripcion                                        |
| -------------------------------------- | -------------------------------------------------- |
| `docker ps -a`                         | listar contenedores (incluido ocultos)             |
| `docker images -a`                     | listar imagenes (incluido ocultas)                 |
| `docker stop nombre_contenedor`        | detener un contenedor                              |
| `docker start nombre_contenedor`       | iniciar un contenedor                              |
| `docker restart nombre_contenedor`     | reiniciar un contenedor                            |
| `docker exec -ti nombre_contendor bash`| entrar a un contenedor                             |
| `docker inspect nombre_contenedor`     | ver configuraciones del contenedor, IP y de más    |

### Eliminar

| Comando                                | Descripcion                                        |
| -------------------------------------- | -------------------------------------------------- |
| `docker rm nombre_contendor`           | eliminar contenedor                                |
| `docker rmi nombre_imagen`             | eliminar imagen                                    |
| `docker rmi $(docker images -q)`       | eliminar todas las imágenes                        |
| `docker rm $(docker ps -a -q)`         | eliminar todos los contenedores                    |
| `docker image prune -a --force`        | eliminar las imagenes no usadas por contenedor     |

### Listar y Eliminar

| Comando                                                       | Descripcion                                        |
| ------------------------------------------------------------- | -------------------------------------------------- |
| `docker volume ls -f dangling=true`                           | listar volúmenes no usados por contenedor          |
| `docker images -f dangling=true`                              | listar imágenes no usadas por contenedor           |
| `docker volume rm $(docker volume ls -f dangling=true -q)`    | borrar volúmenes no usados por contenedor          |
| `docker rmi $(docker images -f dangling=true -q)`             | eliminar imágenes no usadas por contenedor         |

## .dockerignore

| Instruccion                            | Descripcion                                        |
| -------------------------------------- | -------------------------------------------------- |
| `**`                                   | ignorar todos los archivos                         |
| `*/*`                                  | ignorar todos los archivos de cualquier carpeta    |
| `*.*`                                  | ignorar todos los archivos de cualquier formato    |
| `*.txt`                                | ignorar solo archivos con extensión txt            |
| `**/.terraform`                        | ignorar .terraform de cualquier directorio         |
| `terraform.*`                          | ignorar cualquier archivo llamado terraform        |
| `/**`                                  | ignorar todos los archivos                         |
| `!*.tfvars`                            | NO ignorar los archivos con extencion tfvars       |

## Dockerizar una Web APP

1. crea el Dockerfile

    ```dockerfile
    FROM node:14.15-alpine

    LABEL   project="manager-api"
    LABEL   copyright="luisvanegas.com"
    LABEL   version="1.0.0"
    LABEL   release-date="11-abr-2023"
    LABEL   maintainer="angelvamart@hotmail.com"
    LABEL   description="Image with manager-api developed in node"

    RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
    WORKDIR /home/node/app
    COPY package*.json ./

    USER node
    RUN npm install
    COPY --chown=node:node . .

    EXPOSE 2022

    CMD [ "node", "src/server.js" ]
    ```

2. crea el .dockerignore

    ```bash
    node_modules
    e2e
    ```

3. crea la imagen

    ```bash
    docker build -t nombre_imagen . -f Dockerfile
    ```

4. ejecuta la imagen para crear un contenedor

    ```bash
    docker run -d --name nombre_contenedor --restart unless-stopped -p puerto_exponer:puerto_app nombre_imagen
    ```

## Publicar una imagen en Dockerhub

1. crear una cuenta en Dockerhub
2. hacer un docker login

    ```bash
    docker login
    ```

3. hacer push de una imagen

    ```bash
    docker push nombre_imagen
    ```

## Descargar una imagen de Dockerhub

1. hacer un docker login

    ```bash
    docker login
    ```

2. hacer pull de una imagen

    ```bash
    docker pull nombre_imagen
    ```

## ERROR: Docker - Bind for 0.0.0.0:4000 failed: port is already allocated

```bash
service docker stop
```

## Dejar de usar sudo su

```bash
sudo groupadd docker
```

```bash
sudo usermod -aG docker $USER
```

>Nota: Luego reinicia la maquina o el docker service
