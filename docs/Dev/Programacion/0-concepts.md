# Conceptos

## Algoritmos

Un algoritmo ​ es un conjunto de instrucciones o reglas definidas y no-ambiguas, ordenadas y finitas que permite, típicamente, solucionar un problema, realizar un cómputo, procesar datos y llevar a cabo otras tareas o actividades.​

![algoritmo](https://upload.wikimedia.org/wikipedia/commons/b/bd/LampFlowchart-es.svg)

## Diagrama de Flujo

Es una de las formas de representar un algoritmo y du simbologia puede variar.

![smbologia](https://static.platzi.com/media/user_upload/simbolos-diagrama-de-flujo-a942a526-219e-44cf-b5d3-d4d1069b3c30.jpg)

## Pseudocodigo

Es otra forma de representar un algoritmo pero esta por medio de un lenguaje natural. Un ejemplo de este es:

```JAVA
Algoritmo resta
    Leer numero1, numero2
    resultado<-numero1 - numero2
    Escribir resultado
FinAlgoritmo
```

## PSeint

Este es un programa que permite representar algoritmos en forma de Diagramas de flujos y Pseudocodigo. Este lo puedes descargar en: <https://pseint.sourceforge.net/?page=descargas.php>

## Ejercicios PSeint

Estos ejercicios son creados con PSeint y para importarlos en PSeint basta con crear un archivo de extension `.psc` y pegar dentro cada codigo, luego vas a PSeint y lo importas

=== "EJERCICIO 1"

    Crea un programa que sume 2 numeros e imprima su resultado por pantalla.

    Te tienes que preguntar:

    - Que me dan?
    - Que me piden?
    - Que debo de dar?

    Como respuestas, me dan 2 numeros y me piden que los sume y luego que de el resultado.

    ```JAVA
    Algoritmo suma
        Leer numero1, numero2
        resultado<-numero1 + numero2
        Escribir resultado
    FinAlgoritmo
    ```

=== "EJERCICIO 2"

    En un almacén se quiere realizar un descuento del 10% a los clientes cuya compra sea mayor a 50.000, cree un programa que genere el valor total a pagar de acuerdo a la política establecida para el descuento.

    ```JAVA
    Algoritmo descuento
        Leer valorCompra
        Si valorCompra>50000 Entonces
            valorDescuento <- valorCompra*0.10
            valorTotal <- valorCompra-valorDescuento
        SiNo
            valorTotal <- valorCompra
        FinSi
        Escribir valorTotal
    FinAlgoritmo
    ```

=== "EJERCICIO 3"

    Una empresa quiere verificar que sus colaboradores usen el uniforme adecuado según el día que toque. Por ejemplo, si es Lunes tienen que ir con el uniforme azul y si es miercoles, tienen que ir con el uniforme amarillo, de lo contrario no se les puede dar el acceso a la empresa. Para esto te han buscado con el fin de que crees un programa que valide el uniforme del colaborador según el día y que muestre por pantalla si el acceso es aceptado o denegado.
    Debes tener en cuenta que la empresa solo trabaja de lunes a viernes y de acuerdo a los siguientes días se usa un color de uniforme específico:

    - lunes: marron
    - martes: blanco
    - miercoles: marron
    - jueves: verde
    - viernes: marron

    ```JAVA
    Algoritmo Color_de_uniforme
        Leer dia,uniforme
        Segun dia  Hacer
            'lunes':
                Si uniforme='marron' Entonces
                    Escribir 'acceso aceptado'
                SiNo
                    Escribir 'acceso denegado'
                FinSi
            'martes':
                Si uniforme='blanco' Entonces
                    Escribir 'acceso aceptado'
                SiNo
                    Escribir 'acceso denegado'
                FinSi
            'miercoles':
                Si uniforme='marron' Entonces
                    Escribir 'acceso aceptado'
                SiNo
                    Escribir 'acceso denegado'
                FinSi
            'jueves':
                Si uniforme='verde' Entonces
                    Escribir 'acceso aceptado'
                SiNo
                    Escribir 'acceso denegado'
                FinSi
            'viernes':
                Si uniforme='marron' Entonces
                    Escribir 'acceso aceptado'
                SiNo
                    Escribir 'acceso denegado'
                FinSi
            De Otro Modo:
                Escribir 'ingreso un dia no valido'
        FinSegun
    FinAlgoritmo
    ```

    El algoritmo anterior se puede optimizar asi:

    ```JAVA
    Algoritmo Color_de_uniforme_OPTIMIZADO
        Leer dia,uniforme
        Segun dia  Hacer
            'lunes':
                uniformeEsperado <- 'marron'
            'martes':
                uniformeEsperado <- 'blanco'
            'miercoles':
                uniformeEsperado <- 'marron'
            'jueves':
                uniformeEsperado <- 'verde'
            'viernes':
                uniformeEsperado <- 'marron'
            De Otro Modo:
                Escribir 'ingreso un dia no valido'
        FinSegun
        Si uniforme<>uniformeEsperado Entonces
            Escribir 'acceso denegado'
        SiNo
            Escribir 'acceso aceptado'
        FinSi
    FinAlgoritmo
    ```

## Ejercicios Python

=== "EJERCICIO 4"

    La empresa Connect necesita crear un sistema que imprima por pantalla la accion a realizar de acuerdo al estado activo de un semaforo.

    | Estado    | Accion         |
    | --------- | -------------- |
    | `Rojo`    | Detengase      |
    | `Naranja` | Espere         |
    | `Verde`   | Siga su camino |

=== "EJERCICIO 5"

    En un colegio te dan una lista de estudiantes con su primer nombre. El colegio necesita un programa que les imprima los nombres de todos los estudiantes. La lista de estudiantes es la siguiente:

    | Estudiantes |
    | ----------- |
    | Luis        |
    | Agustina    |
    | Pablo       | 
    | Florencia   |
    | Martin      |
    | Liliana     |

=== "EJERCICIO 6"

    En un almacen se quiere realizar un descuento del 10% a los clientes cuya compra sea mayor a 50.000, cree un programa que genere el valor total a pagar de acuerdo a la política establecida para el descuento. adicional, el propietario del almacén quiere que el programa se mantenga abierto y se cierre cuando el lo diga.

=== "EJERCICIO 7"

    Dado el siguiente arreglo: [1, 5, 8, 3, 30, 9, 13], imprimir por pantalla los números impares mayores a 3.

=== "EJERCICIO 8"

    Crear un programa que pida un valor numérico y te diga si el número es par o impar.

=== "EJERCICIO 9"

    Crea una clase perro que pueda caminar, ladrar, comer y que tenga un nombre. luego crea 3 perritos que caminen, un perrito que ladre y 2 perritos que coman.

## Programacion

En programacion usamos distintos lenguajes que requieren de diferentes herramientas para sus desarrollos y ejecuciones, por ejemplo:

- Entornos de Desarrollos 'IDE'
- Compiladores
- Kits de desarrollo

Existen dieferentes tipos de lenguajes:

- **Compilados:** convierte el codigo a un archivo binario que lee el SO. Ej: C#, C GO.
- **Interpretados:** requiere de un programa para leer cada instruccion de codigo en tiempo real. Ej: JS, Python, Ruby,
- **Intermedios:** se compila el codigo en un lenguaje y este se ejecuta en una maquina virtual. Ej: Scala, Java, Kotlin.

Tambien hay nomenclaturas que ayudan a crear codigo mas legible y entendible como:

- **PascalCase:** Se usan para nombrar archivos y clases
- **camelCase:** Se usan para nombrar variables, atributos, funciones y metodos.
- **snake_case:** Se usan para nombrar campos o columnas en una base de datos.

## Ingeniería de Software

- **Ingeniería:** es la aplicación de la ciencia para la resolución de problemas en beneficio de la sociedad

Aplica diferentes normas y métodos que permiten obtener mejores resultados

**Objetivos:**

- Mejorar el diseño de aplicaciones o software.
- Promover mayor calidad al desarrollar aplicaciones complejas.
- Brindar mayor exactitud en los costos de proyectos y tiempo de desarrollo de los mismos.
- Una mejor organización de equipos de trabajo, en el área de desarrollo y mantenimiento de software.
- Detectar a través de pruebas, posibles mejoras para un mejor funcionamiento del software desarrollado.

## Ciclo de Vida del Software (CVDS)

Son las etapas del software

- requerimientos
- análisis y diseño
- desarrollo
- pruebas
- implementación
- mantenimiento

## Modelos de Ciclo de Vida del Software

### Cascada

![cascada](https://static.wixstatic.com/media/fd2cc9_128a79ff8412496d9ee11d4a22d5e286~mv2.png/v1/fill/w_740,h_492,al_c,q_90,usm_0.66_1.00_0.01,enc_auto/fd2cc9_128a79ff8412496d9ee11d4a22d5e286~mv2.png)

### V

![v](https://static.wixstatic.com/media/fd2cc9_122b8f0171ed42a9b980021be2a1decf~mv2.png/v1/fill/w_740,h_567,al_c,lg_1,q_90,enc_auto/fd2cc9_122b8f0171ed42a9b980021be2a1decf~mv2.png)

### Iterativo

![iterativo](https://static.wixstatic.com/media/fd2cc9_410fd994bd3d4262a5dfd5f6c174191f~mv2.png/v1/fill/w_740,h_380,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/fd2cc9_410fd994bd3d4262a5dfd5f6c174191f~mv2.png)

### Incremental

![incremental](https://static.wixstatic.com/media/fd2cc9_2f5b70ca261a483eb32ae647c83bf5b5~mv2.png/v1/fill/w_740,h_382,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/fd2cc9_2f5b70ca261a483eb32ae647c83bf5b5~mv2.png)

### Espiral

![espiral](https://static.wixstatic.com/media/fd2cc9_ed1284d21a354363a2eff370c4823a2c~mv2.png/v1/fill/w_740,h_461,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/fd2cc9_ed1284d21a354363a2eff370c4823a2c~mv2.png)

### Prototipos

![prototipos](https://static.wixstatic.com/media/fd2cc9_601f6e8bc9fc4b5b9f3092b6d3c5c5f8~mv2.png/v1/fill/w_740,h_528,al_c,lg_1,q_90,enc_auto/fd2cc9_601f6e8bc9fc4b5b9f3092b6d3c5c5f8~mv2.png)
