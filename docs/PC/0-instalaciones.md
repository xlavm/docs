# Instalaciones

Estos son todos los programas bases que se requieren para la labor de Programador

## Esenciales

- Chrome
- Mozilla
- Lightshot <https://app.prntscr.com/es/download.html>

    Configurar las teclas de acceso

    ![lightshot](img/lightshot.png)

- 7zip <https://www.7-zip.org/download.html>
- AcrobaDCPortable
- Movavi Screen Capture
- WPS Office
- Zoom
- Teams
- Slack

## Especializados

- Visual Studio Code
- Nodejs
- Postman
- Draw.io (desde microsoft store)
- AWS CLI
- Ubuntu LTS (desde microsoft store)
- Notepad ++
- Java 8 <https://www.java.com/es/download/ie_manual.jsp>

    Verifica la instalacion con: `java -version`

    Si no muestra la version, verifica las variables de entorno

- Pyhton <https://www.python.org/downloads/>

    Realiza una instalacion personalizada y selecciona estos valores:

    ![python](img/python.png)

    Verifica la instalacion con: `pip --version` y `py --version`.

- Android Studio

    Descargar una version de Android SDK Platforms

    1. Android 10.0
    2. Android 12.0 (S)

    Descargar Android SDK Tools:

    1. Build-tools
    2. Command-line-tools
    3. Emulator
    4. Emulator Hypervisor AMD o Emulator Hypervisor Intel (depende de tu procesador)
    5. Platform-tools
    6. USB Driver
    7. Play Services
    8. NDK
    9. CMake
    10. Intel x86 Emulator Accelerator (HAXM installer) (depende de tu procesador)

- Appium Inspector
- Appium

    ```bash
    npm i --location=global appium
    ```
