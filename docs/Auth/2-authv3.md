# Auth V3

Este repositorio contiene 2 proyectos los cuales realizan las funcionalidades de `Registro de usuario (Sign up)`, `Inicio de Sesion (Sign in)` y `Obtencion de informacion (getData)`

- APP Auth (`React Native`)
- WEB Auth (`Angular`)

---

## Arquitectura V3

![k1](imgv3/k1.png)

---

## Mockups

<details><summary>Form - Formulario</summary>

Los valores de los labels, placeholder de los input, buttons y link son parametrizados y de acuerdo a la vista que se invoque, sera mostrado su valor, ejemplo:

- para `sign-in` el titulo seria `Iniciar Sesion`
- para `sign-up` el titulo seria `Registrarse`

Este componente form sera renderizado en cada uno de los componentes correspondientes, `sign-in` y `sign-up`.
<img src="../imgv3/form.png">
</details>

<details><summary>Sign-up - Vista</summary>

<img src="../imgv3/sign-up.png">
</details>

<details><summary>Sign-in - Vista</summary>
<img src="../imgv3/sign-in.png">
</details>

<details><summary>Home - Vista</summary>
<img src="../imgv3/home.png">
</details>

## Tests

<details> <summary>Pruebas E2E automatizadas con Cypress</summary>

Se crean pruebas E2E automatizadas para WEB-Auth con Cypress.

<img src="../imgv3/e2e-test-web.png">
</details>
