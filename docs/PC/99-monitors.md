# Monitores

La referencia es: **LG UltraGear 24GN600**

Esta es la configuracion requerida para aprovechar al maximo los FPS y los Hz que Ofrece.

![1](img/monitores/1.jpeg)

![2](img/monitores/2.jpeg)

![3](img/monitores/3.jpeg)

![4](img/monitores/4.jpeg)

![5](img/monitores/5.jpeg)
