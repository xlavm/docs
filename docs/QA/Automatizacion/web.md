# Web

Estos proyectos de automatizacion cumplen con el patron de diseño `Page Object Model (POM)`.

Tecnologias:

- Pytest (python)
- Robotframework (python)
- Cypress (javascript)

**Documentos de apoyo**

- Pytest:
    1. Selenium: <https://www.selenium.dev/documentation/webdriver/elements/>
- Robotframework (python)
    1. Robot Framework: <https://docs.robotframework.org/docs>
    2. SeleniumLibrary: <https://robotframework.org/SeleniumLibrary/>
    3. Keywords: <https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html>

- Cypress (javascript)
    1. <https://docs.cypress.io/guides/overview/why-cypress>
    2. <https://automationstepbystep.com/>

## Frameworks Base

<https://gitlab.com/lavm/qaa-frameworks/-/tree/main/frameworks-bases/web?ref_type=heads>
