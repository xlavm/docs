# Formularios Reactivos

=== "ts.config.json"

    Cambiar de true a `false` el valor del parametro `strict` en el `compilerOptions`

    ```JSON
    {
      "compilerOptions": {
        "strict": false, 
      }
    }
    ```

=== "app.module.ts"

    ```TS
    import { ReactiveFormsModule } from '@angular/forms';

    imports: [
      ReactiveFormsModule //para formularios reactivos
    ],
    ```

=== "app.component.ts"

    ```TS
    import { Component } from "@angular/core";
    import { FormBuilder, FormGroup, Validators } from "@angular/forms";

    @Component({
      selector: "app-root",
      templateUrl: "./app.component.html",
    })
    export class AppComponent {

      //declaro el formulario
      signUpForm: FormGroup;

      constructor(private formBuilder: FormBuilder) {
        //inicializo el formulario con sus validaciones
        this.signUpForm = this.formBuilder.group({
          name: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(60)]],
          user: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]],
          pass: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(60)]],
          status: ['', [Validators.requiredTrue]],
          type: ['', [Validators.required]]
        });
      }

      //creo los metodos para obtener los controles del formulario
      get name() { return this.signUpForm.get('name') }
      get user() { return this.signUpForm.get('user') }
      get pass() { return this.signUpForm.get('pass') }
      get status() { return this.signUpForm.get('status') }
      get type() { return this.signUpForm.get('type') }

      //creo el metodo muestra los valores digitados por consola
      signUp(): any {
        console.log(this.signUpForm.value);//obtiene la data del form como JSON
      }

      loadInfo() {
        const info = {
          name: "Luis Vanegas",
          user: "xlavm",
          pass: "xlavm",
          status: true,
          type: "standar",
        };
        this.signUpForm.patchValue(info);//modifica la data del form con lo que tiene info
      }
    }
    ```

=== "app.component.html"

    ```HTML
    <!-- Creo el form con su formGroup nombrado igual que en el .ts y el submit llamando al metodo signUp() -->
    <form [formGroup]="signUpForm" (submit)="signUp()">
      <div class="container">
        <div class="mb-3">
          <h2>Registrarse</h2>
        </div>

        <div class="mb-3">
          <!-- especifico el formControlName tal cual lo nombre en el .ts 
          esto hara que el .ts pueda identificar el control e interactuar con su valor-->
          <input type="text" class="form-control" id="name" formControlName="name" placeholder="Nombre">
          <!-- Realizo las validaciones del campo -->
          <div class="error" *ngIf="name.touched">
            <p *ngIf="name.errors?.['required']">
              El nombre es requerido</p>
            <p *ngIf="name.errors?.['minlength']">
              El valor tiene que ser mayor a 3 caracteres</p>
            <p *ngIf="name.errors?.['maxlength']">
              El valor no puede ser tan grande</p>
          </div>
        </div>

        
        <div class="mb-3">
          <input type="text" class="form-control" id="user" formControlName="user" placeholder="Usuario">
          <div class="error" *ngIf="user.touched">
            <p *ngIf="user.errors?.['required']">
              El nombre es requerido</p>
            <p *ngIf="user.errors?.['minlength']">
              El valor tiene que ser mayor a 3 caracteres</p>
            <p *ngIf="user.errors?.['maxlength']">
              El valor no puede ser tan grande</p>
          </div>
        </div>

        <div class="mb-3">
          <input type="password" class="form-control" id="pass" formControlName="pass" placeholder="Contraseña">
          <div class="error" *ngIf="pass.touched">
            <p *ngIf="pass.errors?.['required']">
              El nombre es requerido</p>
            <p *ngIf="pass.errors?.['minlength']">
              El valor tiene que ser mayor a 3 caracteres</p>
            <p *ngIf="pass.errors?.['maxlength']">
              El valor no puede ser tan grande</p>
          </div>
        </div>

        <div class="mb-3">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" id="status" formControlName="status">
            <label class="form-check-label">Activo</label>
          </div>
          <div class="error" *ngIf="status.touched">
            <p *ngIf="status.errors?.['required']">
              Marque la casilla</p>
          </div>
        </div>

        <div class="mb-3">
          <select class="form-select" aria-label="Default select example" formControlName="type">
            <option value="" disabled selected hidden>Seleccione</option>
            <option value="admin">Admin</option>
            <option value="superadmin">Superadmin</option>
            <option value="standar">Standar</option>
          </select>
          <div class="error" *ngIf="type.touched">
            <p *ngIf="type.errors?.['required']">
              Seleccione un tipo</p>
          </div>
        </div>

        <div class="button">
          <button type="button" class="btn btn-dark" id="gargarInfo" (click)="loadInfo()">Cargar Info</button> &nbsp;
          <!-- deshabilito el boton si las validaciones son invalidas -->
          <button type="submit" class="btn btn-dark" id="registrar" [disabled]="signUpForm.invalid">Registrar</button>
        </div>

      </div>
    </form>
    ```

=== "index.html"

    ```HTML
    <!doctype html>
    <html lang="en">
    <head>
      <meta charset="utf-8">
      <title>ReactiveForm</title>
      <base href="/">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href="favicon.ico">
        <!-- agrego css de bootstrap -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    </head>
    <body>
      <app-root></app-root>
      <!-- agrego js de bootstrap -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    </body>
    </html>
    ```

=== "styles.css"

    ```CSS
    .container {
        background-color: whitesmoke;
        margin: 10% auto;/* Centrar */
        padding: 2%;
        width: 20%;
        min-width: 360px;/* segun mobile */
    }

    .button {
        text-align: right;
    }
    
    .error {
        color: brown;
        font-size: 14px;
    }
    ```

## Formularios Reactivos Dinamicos

=== "app.component.ts"

    Modificar el `constructor` agregando el nuevo campo dinamico `skills`

    ```TS
    constructor(private formBuilder: FormBuilder) {
      this.signUpForm = this.formBuilder.group({
        name: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(60)]],
        user: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]],
        pass: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(60)]],
        status: ['', [Validators.requiredTrue]],
        type: ['', [Validators.required]],
        skills: this.formBuilder.array([], [Validators.required]),//agrego el nuevo campo como array
      });
    }
    ```

    Agregar un nuevo `metodo` para obtener el control del campo dinamico

    ```TS
     get skills() { return this.signUpForm.get('skills') as FormArray }//creo el get del nuevo campo array
    ```

    Creo nuevos metodos para `Agregar` y `Remover` campos dinamicos

    ```TS
    //creo el metodo que inicializa el nuevo grupo de skill con sus objetos
    addSkill() {
      this.skills.push(this.formBuilder.group({
        skill: ['', [Validators.required]],
        experience: ['', [Validators.required]],
      }));
    }
    //creo el metodo que elimina el ultimo grupo de skill
    removeSkill() {
      this.skills.removeAt(this.skills.length-1);
    }
    ```

    Modifico el metodo `loadInfo()`

    ```TS
      loadInfo() {
      const info = {
        name: 'Luis Vanegas',
        user: 'xlavm',
        pass: 'xlavm',
        status: true,
        type: 'standar',
        skills: [{
          skill: 'QA Automation',
          experience: '6 Years'
        }]
      };
      this.signUpForm.patchValue(info);
    }
    ```

=== "app.component.html"

    Agrego los componentes html para el formulario dinamico

    ```HTML
    <!-- Agrego el bloque de codigo HTML para cuando quiero agregar una nueva habilidad  -->
    <strong>Habilidades</strong>
    <div class="skills-button">
      <!-- botones para agregar o quitar una habilidad -->
      <button type="button" class="btn btn-dark" id="agregarHabilidad" (click)="addSkill()">+</button>
      <button type="button" class="btn btn-dark" id="agregarHabilidad" (click)="removeSkill()">-</button>
    </div>
    <hr>
    <!-- declaro el formArrayName con el nombre igual al campo del .ts -->
    <!-- recorro cada control del array de skills -->
    <div formArrayName="skills" *ngFor="let skill of skills.controls; let i=index">
      <!-- creo un groupName para cada indice del array con los campos siguientes: -->
      <div [formGroupName]="i">
        <div class="mb-3">
          <!-- campo1 -->
          <input type="text" class="form-control" formControlName="skill" placeholder="Habilidad">
          <!-- validacion del campo1 -->
          <div class="error" *ngIf="skill.get('skill').touched">
            <p *ngIf="skill.get('skill').errors?.['required']">
              La habilidad es requerida</p>
          </div>
        </div>
        <div class="mb-3">
          <!-- campo2 -->
          <input type="text" class="form-control" formControlName="experience" placeholder="Experiencia">
          <!-- validacion del campo2 -->
          <div class="error" *ngIf="skill.get('experience').touched">
            <p *ngIf="skill.get('experience').errors?.['required']">
              La Experiencia es requerida</p>
          </div>
        </div>
        <hr>
      </div>
    </div>
    ```

=== "styles.css"

    Agrego los nuevos estilos para el nuevo HTML

    ```CSS
    .skills-button {
        margin: 2%;
    }

    .skills-button button {
        width: 10%;
        margin: 1%;
    }
    ```
