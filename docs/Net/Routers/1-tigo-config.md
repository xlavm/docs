# Configuraciones Tigo

Modem: `Huawuei HG532E`

## DMZ

Es la zona desmilitarizada, la IP que asociemos a esta zona tendrá todos los puestos habilitados para que el dispositivo con dicha IP que está en la red LAN se comunique con el mundo exterior recibiendo peticiones desde cualquier puerto.

![dmz](img/dmz.png)

*NOTA: el DMZ solo es para cuando queremos que una máquina tenga todos los puertos abiertos*

## Port Forwarding

En esta parte configuramos las máquinas que queremos exponer al mundo exterior y los puertos por donde se comunicarán.

![port-forwarding](img/port-forwarding.png)

## Modo Bridge (Puente)

Cambia los siguientes valores marcados

![bridge1](img/bridge1.png)

![bridge2](img/bridge2.png)