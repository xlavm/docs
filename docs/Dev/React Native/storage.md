# Variable de Sesion

Instala la siguiente dependencia (Entorno Android con npm)

```BASH
npm install @react-native-async-storage/async-storage --save
```

Para importarlo en un componente usa:

```JS
import AsyncStorage from '@react-native-async-storage/async-storage'
```

Para usarlo:

```JS
//crea o modifica una variable
AsyncStorage['variable'] = valor;

//obtiene un valor de la variable
valor = AsyncStorage['variable']

//limpia todas las variables
AsyncStorage.clear()
```

<https://reactnative.dev/docs/asyncstorage#clear>