# AMD Supresor de Sonido

Reduce el ruido del ambiente para que el sonido sea más claro, y tú te puedas concentrar mejor, ya sea que estés en una reunión importante o metido de lleno en un juego competitivo.

<https://www.amd.com/en/technologies/amd-noise-suppression>

## Prerrequisitos

- Drivers actualizados (AMD)
- AMD Software Adrenaline Edition actualizado

## Activacion

1. Ingresa a AMD Adrenaline Edition y selecciona el dispositivo de entrada que usas como microfono

    ![amd](img/supresor/amd.png)

2. Selecciona como dispositivo de salida en las configuraciones de windows el `Microfono (AMD Streaming Audio)

    ![win](img/supresor/win.png)
