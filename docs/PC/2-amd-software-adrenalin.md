# AMD Software Adrenalin

Permite acceder rápidamente a las últimas funciones de software, estadísticas de juego, informes de rendimiento, actualizaciones de controladores, etc.

<https://www.amd.com/es/technologies/software>

## Prerrequisitos

- Drivers actualizados (AMD)
