# Windows 10

Fondos de Pantalla

![rojo](img/fondos-de-pantalla/rojo.png)

![negro](img/fondos-de-pantalla/negro.png)

Estas son las configuraciones que modifico para tener mejor experiencia y rendimiento.

![config](img/win10/config.png)

## Aplicaciones

![apps](img/win10/aplicaciones/Screenshot_1.png)

![apps1](img/win10/aplicaciones/Screenshot_2.png)

![apps2](img/win10/aplicaciones/Screenshot_3.png)

## Dispositivos

![disp](img/win10/dispositivos/Screenshot_1.png)

![disp2](img/win10/dispositivos/Screenshot_2.png)

## Juegos

![game](img/win10/juegos/Screenshot_1.png)

![game2](img/win10/juegos/Screenshot_2.png)

![game3](img/win10/juegos/Screenshot_3.png)

## Personalizacion

![perso](img/win10/personalizacion/Screenshot_1.png)

![perso2](img/win10/personalizacion/Screenshot_2.png)

![perso3](img/win10/personalizacion/Screenshot_3.png)

![perso4](img/win10/personalizacion/Screenshot_4.png)

![perso5](img/win10/personalizacion/Screenshot_5.png)

![perso6](img/win10/personalizacion/Screenshot_6.png)

## Privacidad

![priv](img/win10/privacidad/Screenshot_1.png)

![priv2](img/win10/privacidad/Screenshot_2.png)

![priv3](img/win10/privacidad/Screenshot_3.png)

![priv4](img/win10/privacidad/Screenshot_4.png)

![priv5](img/win10/privacidad/Screenshot_5.png)

![priv6](img/win10/privacidad/Screenshot_6.png)

![priv7](img/win10/privacidad/Screenshot_7.png)

![priv8](img/win10/privacidad/Screenshot_8.png)

![priv9](img/win10/privacidad/Screenshot_9.png)

![priv10](img/win10/privacidad/Screenshot_10.png)

![priv11](img/win10/privacidad/Screenshot_11.png)

![priv12](img/win10/privacidad/Screenshot_12.png)

![priv13](img/win10/privacidad/Screenshot_13.png)

![priv14](img/win10/privacidad/Screenshot_14.png)

![priv15](img/win10/privacidad/Screenshot_15.png)

![priv16](img/win10/privacidad/Screenshot_16.png)

![priv17](img/win10/privacidad/Screenshot_17.png)

![priv18](img/win10/privacidad/Screenshot_18.png)

![priv19](img/win10/privacidad/Screenshot_19.png)

![priv20](img/win10/privacidad/Screenshot_20.png)

![priv21](img/win10/privacidad/Screenshot_21.png)

![priv22](img/win10/privacidad/Screenshot_22.png)

![priv23](img/win10/privacidad/Screenshot_23.png)

![priv24](img/win10/privacidad/Screenshot_24.png)

## Sistema

![sis1](img/win10/sistema/Screenshot_1.png)

![sis2](img/win10/sistema/Screenshot_2.png)

![sis3](img/win10/sistema/Screenshot_3.png)

![sis4](img/win10/sistema/Screenshot_4.png)

![sis5](img/win10/sistema/Screenshot_5.png)

![sis6](img/win10/sistema/Screenshot_6.png)

![sis7](img/win10/sistema/Screenshot_7.png)

![sis8](img/win10/sistema/Screenshot_8.png)

![sis9](img/win10/sistema/Screenshot_9.png)

![sis10](img/win10/sistema/Screenshot_10.png)

![sis11](img/win10/sistema/Screenshot_11.png)

![sis12](img/win10/sistema/Screenshot_12.png)
