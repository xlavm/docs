# Conversiones

## Convertir de Cualquier ... a String

=== "JAVA"

    ```JAVA
    //integer a String
    int entero = 14;
    String cadena = Integer.toString(entero);

    //char a String
    char caracter = 'a';
    String cadena = Character.toString(caracter);

    //double a String
    double decimal = 0.4;
    String cadena = Double.toString(decimal);

    //float a String
    float flotante = 509;
    String cadena = Float.toString(flotante);

    //boolean a String
    boolean buleano = true;
    String cadena = Boolean.toString(buleano);
    ```

=== "JAVASCRIPT"

    ```JS
    //integer a String
    let entero = 14
    let cadena = entero.toString()

    //char a String
    let caracter = 'a'
    let cadena = caracter.toString()

    //double a String
    let double = 789345.7893475943
    let cadena = double.toString()

    //float a String
    let float = 75.7
    let cadena = float.toString()

    //boolean a String
    let boolean = true
    let cadena = boolean.toString()
    ```

=== "PYTHON"

    ```PY
    #integer a String
    entero = 14
    cadena = str(entero)

    #char a String
    caracter = 'a'
    cadena = str(caracter)

    #double a String
    double = 789345.7893475943
    cadena = str(double)

    #float a String
    float = 75.7
    cadena = str(float)

    #boolean a String
    boolean = True
    cadena = str(boolean)
    ```

## Convertir de String a ... Cualquier

=== "JAVA"

    ```JAVA
    //String a integer
    String cadena = "23";
    int entero = Integer.parseInt(cadena);

    //String a char
    String cadena = "una cadena";
    char caracter = cadena.charAt(0); //Solo primer carácter

    //String a double
    String cadena = "3454335.345345";
    double doble = Double.parseDouble(cadena);

    //String a float
    String cadena = "23";
    float flotante = Float.parseFloat(cadena);

    //String a boolean
    String cadena = "luis";
    boolean booleano = Boolean.parseBoolean(cadena);
    ```

=== "JAVASCRIPT"

    ```JS
    //integer a String
    let cadena = '123'
    let entero = parseInt(cadena)

    //char a String
    let cadena = 'a56f'
    let caracter = cadena.charAt(2)

    //double a String
    let cadena = "789345.7893475943"
    let double = parseFloat(cadena)

    //float a String
    let cadena = "75.7"
    let float = parseFloat(cadena)

    //boolean a String
    let cadena = "luis"
    let booleano = new Boolean(cadena)
    ```

=== "PYTHON"

    ```PY
    #integer a String
    cadena = '123'
    entero = int(cadena)

    #char a String
    cadena = 'a56f'
    caracter = cadena[1] #output: 5

    #double a String
    cadena = "789345.7893475943"
    double = float(cadena)

    #float a String
    cadena = "75.7"
    float = float(cadena)

    #boolean a String
    cadena = "luis"
    booleano = bool(cadena)
    ```
