# CMD

El símbolo del sistema es el intérprete de comandos en OS/2 y sistemas basados en Windows NT. Es el equivalente de COMMAND.COM en MS-DOS y sistemas de la familia Windows.

## Comandos

### Comunes

| Comando                            | Descripcion                                        |
| ---------------------------------- | -------------------------------------------------- |
| `dir`                              | listar archivos de un directorio                   |
| `cd /nombre_directorio`            | ir a un directorio                                 |
| `cd ..`                            | regresar al directorio anterior                    |
| `cls`                              | limpiar consola                                    |
| `mkdir nombre_directorio`          | crea un directorio                                 |
