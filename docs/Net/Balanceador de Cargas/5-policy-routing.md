# Politica de Enrutamiento

La idea es definir por cual WAN se van a pasar las peticiones de ciertos grupos de dispositivos.

Prerrequisitos

- Tener un grupo de IP previamente configurado, esto lo ves en la pagina de `Control de Ancho de Banda`

Se cre la Politica indicandole el tipo de peticiones, source, destino y WAN que usara.

![routing-policy](img/rounting-policy.png)
