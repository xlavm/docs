# Internet

Le daremos estabilidad y disminuremos lo mas posible la latencia

## Controlador de Red

Vamos a  `Panel de Control > Redes e Internet > Centro de Redes ... > Cambiar configuracion del adaptador `

![net-controller](img/net-controller.png)

Desactivamos todo de `Administrador de energia`

![net-controller](img/net-controller2.png)

## DNS Servers [ESTE PASO NO ES MUY SEGURO]

Los siguientes son DNS reconocidos que dan la siguiente latencia para Colombia

| Primary  | Secondary         | Latency  | Empresa |
|----------|-------------------|----------| ------- |
| 9.9.9.9  | 149.112.112.112   | 13 ms    | QUAD9   |
| 8.8.8.8  | 8.8.4.4           | 17 ms    | Google  |

Haciendo `ping` podemos verificar la latencia de los servers y escoger el que menor latencia tiene. En mi caso:

![ping](img/ping.png)

El server DNS que me genera mejor latencia es: `QUAD9 Primary: 9.9.9.9, Secondary: 149.112.112.112`

Se cambia el DNS Server en `Panel de Control > Redes e Internet > Centro de Redes ... > Cambiar configuracion del adaptador `

![dns-config](img/dns-config.png)
