# Compartir Mouse y Teclado

Esto ayudara a usar un solo mouse y un solo teclado en diferentes computadores al tiempo.

## Opciones para compartir entre Win y Mac

### Barrier

Se descarga desde: <https://github.com/debauchee/barrier/releases/tag/v2.3.4>

1. Se configura el server (Windows)

    ![win](img/compartir/win.png)

2. Se configura el cliente (Mac)

    ![mac](img/compartir/mac.png)

3. Configuraciones adicionales

    ![config1](img/compartir/config1.png)

    ![config2](img/compartir/config2.png)

4. Hacer que windows abra al iniciar

    Presiona la tecla de Windows + R y copia `shell:startup` y le das en aceptar, luego se abre la siguiente ventana y ahi pegas el acceso directo de barrier asi

    ![inicio](img/compartir/inicio.png)

### Shared Mouse

<https://www.sharemouse.com/>

### Across

<https://download.acrosscenter.com/>

## Opcion para compartir entre Win y Win

Esta es una solución propia de Microsoft

### Microsoft Garage Mouse without Borders

<https://www.microsoft.com/en-us/download/details.aspx?id=35460>

![win2win](img/compartir/wintowin.png)

La security key es la que se comparte con el cliente. Nota: hay que descargar también en el cliente.
